package com.rg.one_note.fragments;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.biometrics.BiometricPrompt;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import android.os.CancellationSignal;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rg.one_note.Constants.Constants;
import com.rg.one_note.DataBase.DBAdapter;
import com.rg.one_note.Drawer.Drawer_Activity;
import com.rg.one_note.R;
import com.rg.one_note.Utility.Utilities;

import static android.content.Context.KEYGUARD_SERVICE;


public class SettingFragment extends Fragment implements View.OnClickListener{

    private Drawer_Activity drawerActivity;
    private DrawerLayout drawer;
    private ImageView ic_hamberger;
    private TextView tvClearBin,tvClearCompletedTasks,tvPrivacyPolicy,tvTermsAndConditions,tvenable_fingerprint;
    private DBAdapter dbAdapter;
    private Context context;
    private CheckBox fingerprint_chkbox;
    private CancellationSignal cancellationSignal;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private  boolean isFingerprintEnabled;

    public SettingFragment(Drawer_Activity drawer_activity) {
        this.drawerActivity = drawer_activity;
        drawer = drawer_activity.getDrawer();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        initViews(view);
        statusBarColor(getString(R.string.color_Cream));
        setClickListeners();
        initSharedPref();
        isFingerprintEnabled = sharedPreferences.getBoolean(Constants.ISFRINGERPRINTENABLED,false);
        if(isFingerprintEnabled)
        {
            fingerprint_chkbox.setChecked(true);
        }
        else
        {
            fingerprint_chkbox.setChecked(false);
        }
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        dbAdapter = new DBAdapter(context);
        this.context = context;
    }

    public void setClickListeners() {
        ic_hamberger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        tvClearCompletedTasks.setOnClickListener(this);
        tvClearBin.setOnClickListener(this);
        tvPrivacyPolicy.setOnClickListener(this);
        tvTermsAndConditions.setOnClickListener(this);
        tvenable_fingerprint.setOnClickListener(this);
        fingerprint_chkbox.setOnClickListener(this);

    }

    private void initViews(View view)
    {
        ic_hamberger = view.findViewById(R.id.ic_hamberger);
        tvClearBin = view.findViewById(R.id.tvClearBin);
        tvClearCompletedTasks = view.findViewById(R.id.tvClearCompletedTask);
        tvPrivacyPolicy = view.findViewById(R.id.tvPrivacyPolicy);
        tvTermsAndConditions = view.findViewById(R.id.tvTermsAndConditions);
        tvenable_fingerprint = view.findViewById(R.id.tvEnable_FingerPrint);
        fingerprint_chkbox = view.findViewById(R.id.checkBox_Enable_fingerprint);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onClick(View view) {


        switch (view.getId())
        {
            case R.id.tvClearBin:
            {
                showDialogForBin();
                break;
            }

            case R.id.tvClearCompletedTask:
            {
                showDialogForTaks();
                break;
            }


            case R.id.tvPrivacyPolicy:
            {
                Uri uri = Uri.parse("https://tasks-notes.flycricket.io/privacy.html");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            }

            case R.id.tvTermsAndConditions:
            {
                Uri uri = Uri.parse("https://tasks-notes.flycricket.io/terms.html");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
                break;
            }

            case R.id.tvEnable_FingerPrint:
            case R.id.checkBox_Enable_fingerprint: {
                authenticateUser();
                break;
            }
            default:
            {
                break;
            }
        }
    }


    private void clearBin()
    {
        dbAdapter.clearDataFromBinTable();
    }

    private void clearCompletedTasks()
    {
        dbAdapter.clearDataFromCompletedTask();
    }

    public void showDialogForBin()
    {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context,R.style.MyDialogTheme);
        builder1.setMessage("This will delete all notes from Bin! \n Continue?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        clearBin();
                        Utilities.showToast(context,"Cleared Bin");
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }

    public void showDialogForTaks()
    {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context,R.style.MyDialogTheme);
        builder1.setMessage("This will delete all completed Task! \n Continue?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        clearCompletedTasks();
                        Utilities.showToast(context,"Cleared Completed Tasks.");
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }

    private void notifyUser(String message) {
        Toast.makeText(context,
                message,
                Toast.LENGTH_LONG).show();
    }

    private Boolean checkBiometricSupport() {

        KeyguardManager keyguardManager =
                (KeyguardManager) context.getSystemService(KEYGUARD_SERVICE);

        PackageManager packageManager = context.getPackageManager();

        if (!keyguardManager.isKeyguardSecure()) {
            notifyUser("Lock screen security not enabled in Settings");
            return false;
        }

        if (ActivityCompat.checkSelfPermission(context,
                Manifest.permission.USE_BIOMETRIC) !=
                PackageManager.PERMISSION_GRANTED) {

            notifyUser("Fingerprint authentication permission not enabled");
            return false;
        }

        if (packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT))
        {
            return true;
        }

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private BiometricPrompt.AuthenticationCallback getAuthenticationCallback() {

        return new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              CharSequence errString) {

                if(BiometricPrompt.BIOMETRIC_ERROR_NO_BIOMETRICS == errorCode)
                {
                    notifyUser("You dont have any fingerprints enrolled in your phone settings.");
                    fingerprint_chkbox.setChecked(false);
                }

                if(BiometricPrompt.BIOMETRIC_ERROR_USER_CANCELED == errorCode)
                {
                    fingerprint_chkbox.setChecked(false);
                }

                if(BiometricPrompt.BIOMETRIC_ERROR_LOCKOUT == errorCode)
                {
                    notifyUser("Too many wrong attempts, Try later");
                    fingerprint_chkbox.setChecked(false);
                }

                super.onAuthenticationError(errorCode, errString);
            }

            @Override
            public void onAuthenticationHelp(int helpCode,
                                             CharSequence helpString) {
                super.onAuthenticationHelp(helpCode, helpString);
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }

            @Override
            public void onAuthenticationSucceeded(
                    BiometricPrompt.AuthenticationResult result) {
                notifyUser("Fingerprint verification enabled");
                fingerprint_chkbox.setChecked(true);
                editor.putBoolean(Constants.ISFRINGERPRINTENABLED,true);
                editor.apply();
                isFingerprintEnabled = true;

                super.onAuthenticationSucceeded(result);
            }
        };
    }

    private CancellationSignal getCancellationSignal() {

        cancellationSignal = new CancellationSignal();
        cancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener() {
            @Override
            public void onCancel() {
                notifyUser("Cancelled via signal");
            }
        });
        return cancellationSignal;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public void authenticateUser() {

        if(isFingerprintEnabled)
        {
            editor.putBoolean(Constants.ISFRINGERPRINTENABLED,false);
            editor.apply();
            Utilities.showToast(context,"FingerPrint Verification disabled");
            isFingerprintEnabled = false;
            fingerprint_chkbox.setChecked(false);
            return;
        }
        BiometricPrompt biometricPrompt = new BiometricPrompt.Builder(context)
                .setTitle("Confirm your fingerprint to Continue.")
                .setDescription("Fingerprint verification will be enabled after confirmed.")
                .setNegativeButton("Cancel", context.getMainExecutor(),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                fingerprint_chkbox.setChecked(false);
                            }
                        })
                .build();
        biometricPrompt.authenticate(getCancellationSignal(), context.getMainExecutor(), getAuthenticationCallback());
    }

    private void initSharedPref()
    {
        sharedPreferences = getActivity().getSharedPreferences(Constants.ISFRINGERPRINTENABLED, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void statusBarColor(String colorCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(colorCode));
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

}