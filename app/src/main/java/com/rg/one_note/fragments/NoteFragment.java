package com.rg.one_note.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.rg.one_note.Constants.Constants;
import com.rg.one_note.DataBase.DBAdapter;
import com.rg.one_note.Drawer.Drawer_Activity;
import com.rg.one_note.Helper.SimpleItemTouchHelperCallback;
import com.rg.one_note.Interfaces.RecyclerViewClickListener;
import com.rg.one_note.R;
import com.rg.one_note.activity.Create_Edit_Note_Activity;
import com.rg.one_note.adapters.NoteAdapter;

import java.util.ArrayList;
import java.util.List;

import Models.Note;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class NoteFragment extends Fragment implements RecyclerViewClickListener , View.OnClickListener {

    private LinearLayout toolbar, borderEditText,llnoNotes;
    private RelativeLayout rlRootLayout;
    private ImageView ic_hamberger, ic_light;
    private Drawer_Activity drawerActivity;
    private DrawerLayout drawer;
    private RecyclerView rvNoteList;
    private FloatingActionButton   fabCreateNote;
    private  Context context;
    private DBAdapter dbAdapter;
    private NoteAdapter noteAdapter;
    private long lastClickTime = 0;
    private boolean isDark = true;
    public static List<Note> noteList;
    private View view;
    private EditText etSearch;
    private List<Note> filteredData;
    private boolean isInSearchMode;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private TextView tvEmptyList;


    public NoteFragment()
    {

    }

    public NoteFragment(Drawer_Activity drawer_activity)
    {
        this.drawerActivity = drawer_activity;
        drawer = drawer_activity.getDrawer();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        dbAdapter = new DBAdapter(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       view =  inflater.inflate(R.layout.fragment_note,container,false);
            initViews(view);
        initSharedPref();
        setClickListeners();

        isDark = sharedPreferences.getBoolean(Constants.IS_LIGHT_THEME,false);
        switchTheme();

        noteList =dbAdapter.getAllNotes();
        filteredData = noteList;

        if(noteList.size() ==0)
        {
            rvNoteList.setVisibility(View.GONE);
            llnoNotes.setVisibility(View.VISIBLE);
        }
        else
        {
            rvNoteList.setVisibility(View.VISIBLE);
            llnoNotes.setVisibility(View.GONE);
        }



      setAdapter(new StaggeredGridLayoutManager(2,LinearLayoutManager.VERTICAL));
        attachItemTouchListeners();


        return view;
    }

    public void initViews(View view)
    {
        toolbar = view.findViewById(R.id.toolbar_custom);
        ic_hamberger = view.findViewById(R.id.ic_hamberger);
        rvNoteList = view.findViewById(R.id.rvNotesList);
        fabCreateNote = view.findViewById(R.id.fabCreateNote);
        ic_light = view.findViewById(R.id.ic_light);
        rlRootLayout = view.findViewById(R.id.rlRootLayout);
        etSearch =view.findViewById(R.id.etSearch);
        borderEditText = view.findViewById(R.id.borderEditText);
        llnoNotes = view.findViewById(R.id.llNoNotes);
        tvEmptyList = view.findViewById(R.id.tvEmptyList);

    }

    @Override
    public void onResume() {
        noteAdapter.updateAdapter(noteList);
        etSearch.setText(null);
        etSearch.clearFocus();
        isInSearchMode = false;
        if(noteList.size() ==0)
        {
            rvNoteList.setVisibility(View.GONE);
            llnoNotes.setVisibility(View.VISIBLE);
        }
        else
        {
            rvNoteList.setVisibility(View.VISIBLE);
            llnoNotes.setVisibility(View.GONE);
        }
        super.onResume();
    }

    public void setAdapter(RecyclerView.LayoutManager layoutManager)
    {
         noteAdapter =new NoteAdapter(context,noteList,this);
        rvNoteList.setLayoutManager(layoutManager);
        rvNoteList.setAdapter(noteAdapter);
    }


    public void setClickListeners()
    {
        ic_hamberger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });


        fabCreateNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
                    return;
                }

                lastClickTime = SystemClock.elapsedRealtime();
                startEdit_CreateActivity(true,null);

            }
        });

        ic_light.setOnClickListener(this);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filter(etSearch.getText().toString());
                isInSearchMode = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void startEdit_CreateActivity(boolean isNewNote,Note note)
    {
        Intent intent = new Intent(getContext(), Create_Edit_Note_Activity.class);
        if(!isNewNote)
        {
            intent.putExtra("NoteObject", note);
            Log.d("intent", "startEdit_CreateActivity: " + note.toString());
        }
        startActivityForResult(intent, 1);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){

                switch (data.getStringExtra(Constants.ACTION_NOTE))
                {
                    case Constants.IS_NEW_NOTE:
                    {
                        Snackbar.make(drawer, "Your note is saved.", Snackbar.LENGTH_SHORT).show();
                        break;
                    }
                    case Constants.IS_EDITED_NOTE:
                    {
                        Snackbar.make(drawer, "Your changes are saved.", Snackbar.LENGTH_SHORT).show();
                        break;
                    }
                    case Constants.IS_DELETED_NOTE:
                    {
                        Snackbar.make(drawer, "Note moved to bin.", Snackbar.LENGTH_SHORT).show();
                        break;
                    }

                    default:
                    {
                        break;
                    }

                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }


    @Override
    public void rowItemClicked(View v , int position) {
        Note note;
        if(isInSearchMode)
        {
             note = filteredData.get(position);
        }
        else
        {
             note = noteList.get(position);
        }

        startEdit_CreateActivity(false,note);

    }

    @Override
    public void requestDrag(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    public void checkBoxClicked(View v, int position) {

    }

    @Override
    public void onPause() {
        for(Note note : noteList)
        {
            long position = getindexByproperty(note.getNote_id());
            dbAdapter.addDraggedPosition(note,position);
        }
        super.onPause();
    }

    private long getindexByproperty(long noteID) {
        for (int i = 0; i < NoteFragment.noteList.size(); i++) {
            if (NoteFragment.noteList.get(i).getNote_id() == noteID) {
                return i;
            }
        }

        return -1;
    }

    @Override
    public void onDestroy() {
//        dbAdapter.clearDataFromNoteTable();
//        dbAdapter.addAllDataFromListToNoteTable(noteList);
//        noteList = dbAdapter.getAllNotes();
        super.onDestroy();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.ic_light:
            {
                switchTheme();
                break;
            }

            default:
            {
                break;
            }
        }
    }

    public void attachItemTouchListeners()
    {
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(noteAdapter,false);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(rvNoteList);
    }

    private void filter(String text)
    {
        if(text.trim().equals(""))
        {
            filteredData = noteList;
            SimpleItemTouchHelperCallback.draggable = true;
            noteAdapter.updateAdapter(filteredData);
            return;
        }
        else
        {
            SimpleItemTouchHelperCallback.draggable = false;
        }

        List<Note> filteredList = new ArrayList<Note>();

        for(Note note : noteList)
        {
            if(note.getTitle() != null && note.getContent() != null)
            {
                if(note.getTitle().toLowerCase().contains(text.trim().toLowerCase()) || note.getContent().toLowerCase().contains(text.trim().toLowerCase()))
                {
                    filteredList.add(note);
                }
            }

        }

        filteredData = filteredList;
        noteAdapter.updateAdapter(filteredData);
    }

    private void switchTheme()
    {
        if(isDark)
        {
            ic_hamberger.setImageResource(R.drawable.ic_hamberger_dark);
            ic_light.setImageResource(R.drawable.ic_bulb_dark);
            rlRootLayout.setBackgroundColor(getResources().getColor(R.color.color_Cream));
            etSearch.setTextColor(getResources().getColor(R.color.color_Black));
            etSearch.setHintTextColor(getResources().getColor(R.color.color_Grey_dark));
            borderEditText.setBackground((getResources().getDrawable(R.drawable.border_chamfer_dark)));
            fabCreateNote.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_Black)));
            statusBarColor(getString(R.string.color_Cream));
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            tvEmptyList.setTextColor(getResources().getColor(R.color.color_Black));
            isDark = false;
            editor.putBoolean(Constants.IS_LIGHT_THEME,true);
            editor.apply();
        }
        else
        {
            ic_hamberger.setImageResource(R.drawable.ic_hamberger_light);
            ic_light.setImageResource(R.drawable.ic_bulb_light);
            rlRootLayout.setBackgroundColor(getResources().getColor(R.color.color_Black));
            etSearch.setTextColor(getResources().getColor(R.color.color_White));
            etSearch.setHintTextColor(getResources().getColor(R.color.color_Cream));
            borderEditText.setBackground((getResources().getDrawable(R.drawable.border_chamfer_light)));
            fabCreateNote.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_Cream)));
            statusBarColor(getString(R.string.color_Black));
            View decorView = getActivity().getWindow().getDecorView(); //set status background black
            decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            tvEmptyList.setTextColor(getResources().getColor(R.color.color_Cream));
            isDark = true;
            editor.putBoolean(Constants.IS_LIGHT_THEME,false);
            editor.apply();
        }

    }

    private void initSharedPref()
    {
        sharedPreferences = getActivity().getSharedPreferences(Constants.THEME_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void statusBarColor(String colorCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(colorCode));
        }
    }


}
