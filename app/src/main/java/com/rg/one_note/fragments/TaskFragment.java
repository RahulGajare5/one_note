package com.rg.one_note.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.rg.one_note.Constants.Constants;
import com.rg.one_note.DataBase.DBAdapter;
import com.rg.one_note.Drawer.Drawer_Activity;
import com.rg.one_note.Helper.SimpleItemTouchHelperCallback;
import com.rg.one_note.Interfaces.RecyclerViewClickListener;
import com.rg.one_note.R;
import com.rg.one_note.activity.TaskActivity;
import com.rg.one_note.adapters.NoteAdapter;
import com.rg.one_note.adapters.TaskAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import Models.Note;
import Models.Task;


public class TaskFragment extends Fragment implements RecyclerViewClickListener, View.OnClickListener {

    private LinearLayout toolbar, borderEditText,llnoTasks;
    private RelativeLayout rlRootLayout;
    private ImageView ic_hamberger, ic_light;
    private Drawer_Activity drawerActivity;
    private DrawerLayout drawer;
    private RecyclerView rvTaskList;
    private FloatingActionButton fabCreateTask;
    private  Context context;
    private DBAdapter dbAdapter;
    private TaskAdapter taskAdapter;
    private long lastClickTime = 0;
    private boolean isDark = true;
    public static List<Task> taskList = Collections.emptyList();
    public static List<Task> taskListCompleted = Collections.emptyList();
    private View view;
    private EditText etSearch;
    private List<Task> filteredData;
    private boolean isInSearchMode;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private ItemTouchHelper touchHelper;
    private Button btnToDo, btnCompleted;
    private boolean isCompletedSelected = false;
    private TextView noTasks;

    public TaskFragment()
    {

    }

    public TaskFragment(Drawer_Activity drawer_activity) {
        this.drawerActivity = drawer_activity;
        drawer = drawer_activity.getDrawer();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        dbAdapter = new DBAdapter(context);
        this.context = context;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_task, container, false);
        initViews(view);
        initSharedPref();
        setClickListeners();
        isDark = sharedPreferences.getBoolean(Constants.IS_LIGHT_THEME,false);
        switchTheme();
        taskList = dbAdapter.getAllTask();
        taskListCompleted = dbAdapter.getAllTaskFromCompleted();
        if(taskList.size() ==0)
        {
            rvTaskList.setVisibility(View.GONE);
            llnoTasks.setVisibility(View.VISIBLE);

        }
        else
        {
            rvTaskList.setVisibility(View.VISIBLE);
            llnoTasks.setVisibility(View.GONE);
        }

            noTasks.setText("You dont have any ToDo Tasks");
        setAdapter(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        attachItemTouchListeners();
        return  view;
    }

    public void setAdapter(RecyclerView.LayoutManager layoutManager)
    {
        taskAdapter =new TaskAdapter(context,taskList,this);
        rvTaskList.setLayoutManager(layoutManager);
        rvTaskList.setAdapter(taskAdapter);
    }
    public void initViews(View view)
    {
        toolbar = view.findViewById(R.id.toolbar_custom);
        ic_hamberger = view.findViewById(R.id.ic_hamberger);
        rvTaskList = view.findViewById(R.id.rvTaskList);
        fabCreateTask = view.findViewById(R.id.fabCreateNote);
        ic_light = view.findViewById(R.id.ic_light);
        rlRootLayout = view.findViewById(R.id.rlRootLayout);
        etSearch =view.findViewById(R.id.etSearch);
        borderEditText = view.findViewById(R.id.borderEditText);
        btnToDo = view.findViewById(R.id.btnToDo);
        btnCompleted = view.findViewById(R.id.btnCompleted);
        llnoTasks = view.findViewById(R.id.llNoTasks);
        noTasks = view.findViewById(R.id.notasks);
        etSearch.setHint("Search Tasks");

    }

    public void setClickListeners()
    {
        ic_hamberger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });


        fabCreateTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
                    return;
                }

                lastClickTime = SystemClock.elapsedRealtime();
                startTaskActivity(true,null);

            }
        });

        ic_light.setOnClickListener(this);
        btnCompleted.setOnClickListener(this);
        btnToDo.setOnClickListener(this);


        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filter(etSearch.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }
    private void filter(String text)
    {
        List<Task> listToSearch;
        if(isCompletedSelected)
        {
         listToSearch = taskListCompleted;
        }
        else
        {
            listToSearch = taskList;
        }
        if(text.trim().equals(""))
        {
            if(isCompletedSelected)
            {
                filteredData = taskListCompleted;
                isInSearchMode = false;
            }
            else {
                filteredData = taskList;
                isInSearchMode = false;
            }
            SimpleItemTouchHelperCallback.draggable = true;
            taskAdapter.updateAdapter(filteredData,isCompletedSelected,isInSearchMode);
            return;
        }
        else
        {
            SimpleItemTouchHelperCallback.draggable = false;
        }

        List<Task> filteredList = new ArrayList<Task>();

        for(Task task : listToSearch)
        {
            if(task.getContent() != null)
            {
                if(task.getContent().toLowerCase().contains(text.trim().toLowerCase()))
                {
                    filteredList.add(task);
                }
            }

        }

        filteredData = filteredList;
        isInSearchMode= true;
        taskAdapter.updateAdapter(filteredData,isCompletedSelected,isInSearchMode);
    }

    private void startTaskActivity(boolean isNewTask, Task task)
    {
        Intent intent = new Intent(getContext(), TaskActivity.class);
        if(!isNewTask)
        {
            intent.putExtra("TaskObject", task);
        }
        startActivityForResult(intent, 2);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.btnToDo:
            {
                btnToDo.setBackgroundColor(context.getResources().getColor(R.color.color_Yellow));
                btnCompleted.setBackgroundColor(context.getResources().getColor(R.color.color_White));
                isCompletedSelected = false;
                taskAdapter.updateAdapter(taskList,isCompletedSelected,isInSearchMode);
                if(taskList.size() ==0)
                {
                    rvTaskList.setVisibility(View.GONE);
                    llnoTasks.setVisibility(View.VISIBLE);

                }
                else
                {
                    rvTaskList.setVisibility(View.VISIBLE);
                    llnoTasks.setVisibility(View.GONE);
                }
                    noTasks.setText("You dont have any ToDo Tasks");
                etSearch.setText(null);
                etSearch.clearFocus();
                break;
            }

            case R.id.btnCompleted:
            {
                btnCompleted.setBackgroundColor(context.getResources().getColor(R.color.color_Yellow));
                btnToDo.setBackgroundColor(context.getResources().getColor(R.color.color_White));
                isCompletedSelected = true;
                taskAdapter.updateAdapter(taskListCompleted,isCompletedSelected,isInSearchMode);
                if(taskListCompleted.size() ==0)
                {
                    rvTaskList.setVisibility(View.GONE);
                    llnoTasks.setVisibility(View.VISIBLE);

                }
                else
                {
                    rvTaskList.setVisibility(View.VISIBLE);
                    llnoTasks.setVisibility(View.GONE);
                }
                noTasks.setText("You dont have any completed Tasks");
                etSearch.setText(null);
                etSearch.clearFocus();

                break;
            }
            case R.id.ic_light:
            {
                switchTheme();
                break;
            }
            default:
            {
                break;
            }
        }

    }

    @Override
    public void rowItemClicked(View v, int position) {
        Task task;
        if(isInSearchMode)
        {
            task = filteredData.get(position);
        }
        else if(isCompletedSelected)
        {
            task = taskListCompleted.get(position);
        }
        else
        {
            task = taskList.get(position);
        }

        startTaskActivity(false,task);
    }

    @Override
    public void checkBoxClicked(View v, int position) {
        Task task;
        if(isCompletedSelected)
        {
            task = taskListCompleted.get(position);
            task.setIsCompleted(0);
            dbAdapter.deleteTaskFromCompleted(task);
            int index = getindexByproperty(task.getTask_id(),taskListCompleted);
            taskListCompleted.remove(index);
            task.setTask_id(null);
            long row = dbAdapter.insertTask(task);
            task.setTask_id(row);
            taskList.add(0,task);
            taskAdapter.updateAdapter(taskListCompleted,isCompletedSelected,isInSearchMode);
            Snackbar.make(drawer, "Task moved to To Do.", Snackbar.LENGTH_SHORT).show();
            if(taskListCompleted.size() ==0)
            {
                rvTaskList.setVisibility(View.GONE);
                llnoTasks.setVisibility(View.VISIBLE);

            }
            else
            {
                rvTaskList.setVisibility(View.VISIBLE);
                llnoTasks.setVisibility(View.GONE);
            }
            noTasks.setText("You dont have any completed Tasks");

        }
        else
        {
            task = taskList.get(position);
            task.setIsCompleted(1);
            dbAdapter.deleteTask(task);
            int index = getindexByproperty(task.getTask_id(),taskList);
            taskList.remove(index);
            task.setTask_id(null);
            long row = dbAdapter.insertTaskInCompleted(task);
            task.setTask_id(row);
            taskListCompleted.add(0,task);
            taskAdapter.updateAdapter(taskList,isCompletedSelected,isInSearchMode);
            Snackbar.make(drawer, "Task moved to Completed.", Snackbar.LENGTH_SHORT).show();
            if(taskList.size() ==0)
            {
                rvTaskList.setVisibility(View.GONE);
                llnoTasks.setVisibility(View.VISIBLE);

            }
            else
            {
                rvTaskList.setVisibility(View.VISIBLE);
                llnoTasks.setVisibility(View.GONE);
            }

            noTasks.setText("You dont have any ToDo Tasks");
        }

    }


    @Override
    public void requestDrag(RecyclerView.ViewHolder viewHolder) {
        touchHelper.startDrag(viewHolder);
    }


    private int getindexByproperty(long taskID,List<Task> taskList) {
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getTask_id() == taskID) {
                return i;
            }
        }

        return -1;
    }

    public void attachItemTouchListeners()
    {
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(taskAdapter,true);
         touchHelper = new ItemTouchHelper(callback);

        touchHelper.attachToRecyclerView(rvTaskList);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){

                switch (data.getStringExtra(Constants.ACTION_TASK))
                {
                    case Constants.IS_NEW_TASK:
                    {
                        Snackbar.make(drawer, "Your Task is saved.", Snackbar.LENGTH_SHORT).show();
                        break;
                    }
                    case Constants.IS_EDITED_TASK:
                    {
                        Snackbar.make(drawer, "Your changes are saved.", Snackbar.LENGTH_SHORT).show();
                        break;
                    }
                    case Constants.IS_COMPLETED_TASK:
                    {
                        Snackbar.make(drawer, "Task moved to Completed.", Snackbar.LENGTH_SHORT).show();
                        break;
                    }

                    case Constants.IS_PERMANENT_DELETED_TASK:
                    {
                        Snackbar.make(drawer, "Task Deleted.", Snackbar.LENGTH_SHORT).show();
                        break;
                    }

                    default:
                    {
                        break;
                    }

                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public void onPause() {
        for(Task task : taskList)
        {
            long position = getindexByproperty(task.getTask_id(),taskList);
            dbAdapter.addDraggedPosition(task,position);
        }
        super.onPause();
    }

    @Override
    public void onResume() {

        taskAdapter.updateAdapter(taskList,isCompletedSelected,isInSearchMode);
        taskAdapter.updateAdapter(taskListCompleted,isCompletedSelected,isInSearchMode);
        etSearch.setText(null);
        etSearch.clearFocus();
        isInSearchMode = false;
        if(!isCompletedSelected)
        {
            if(taskList.size() ==0)
            {
                rvTaskList.setVisibility(View.GONE);
                llnoTasks.setVisibility(View.VISIBLE);

            }
            else
            {
                rvTaskList.setVisibility(View.VISIBLE);
                llnoTasks.setVisibility(View.GONE);
            }

            noTasks.setText("You dont have any ToDo Tasks");
        }

        super.onResume();
    }

    private void switchTheme()
    {
        if(isDark)
        {
            ic_hamberger.setImageResource(R.drawable.ic_hamberger_dark);
            ic_light.setImageResource(R.drawable.ic_bulb_dark);
            rlRootLayout.setBackgroundColor(getResources().getColor(R.color.color_Cream));
            etSearch.setTextColor(getResources().getColor(R.color.color_Black));
            etSearch.setHintTextColor(getResources().getColor(R.color.color_Grey_dark));
            borderEditText.setBackground((getResources().getDrawable(R.drawable.border_chamfer_dark)));
            fabCreateTask.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_Black)));
            statusBarColor(getString(R.string.color_Cream));
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            noTasks.setTextColor(getResources().getColor(R.color.color_Black));
            isDark = false;
            editor.putBoolean(Constants.IS_LIGHT_THEME,true);
            editor.apply();
        }
        else
        {
            ic_hamberger.setImageResource(R.drawable.ic_hamberger_light);
            ic_light.setImageResource(R.drawable.ic_bulb_light);
            rlRootLayout.setBackgroundColor(getResources().getColor(R.color.color_Black));
            etSearch.setTextColor(getResources().getColor(R.color.color_Cream));
            etSearch.setHintTextColor(getResources().getColor(R.color.color_Cream));
            borderEditText.setBackground((getResources().getDrawable(R.drawable.border_chamfer_light)));
            fabCreateTask.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.color_Cream)));
            statusBarColor(getString(R.string.color_Black));
            View decorView = getActivity().getWindow().getDecorView(); //set status background black
            decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            noTasks.setTextColor(getResources().getColor(R.color.color_Cream));
            isDark = true;
            editor.putBoolean(Constants.IS_LIGHT_THEME,false);
            editor.apply();
        }

    }

    private void initSharedPref()
    {
        sharedPreferences = getActivity().getSharedPreferences(Constants.THEME_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void statusBarColor(String colorCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(colorCode));
        }
    }
}
