package com.rg.one_note.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.rg.one_note.Constants.Constants;
import com.rg.one_note.DataBase.DBAdapter;
import com.rg.one_note.Drawer.Drawer_Activity;
import com.rg.one_note.Helper.SimpleItemTouchHelperCallback;
import com.rg.one_note.Interfaces.RecyclerViewClickListener;
import com.rg.one_note.R;
import com.rg.one_note.activity.Bin_Activity;
import com.rg.one_note.adapters.NoteAdapter;

import java.util.ArrayList;
import java.util.List;

import Models.Note;

public class BinFragment extends Fragment implements RecyclerViewClickListener {
    private LinearLayout toolbar, borderEditText,llnoNotes;
    private RelativeLayout rlRootLayout;
    private ImageView ic_hamberger,ic_light;
    private Drawer_Activity drawerActivity;
    private DrawerLayout drawer;
    private RecyclerView rvNoteList;
    private Context context;
    private DBAdapter dbAdapter;
    private NoteAdapter noteAdapter;
    public static List<Note> binNoteList;
    private boolean isDark = true;
    private boolean isInSearchMode;
    private EditText etSearch;
    private List<Note> filteredData;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private TextView tvEmptyList;

    public BinFragment(Drawer_Activity drawer_activity)
    {
        this.drawerActivity = drawer_activity;
        drawer = drawer_activity.getDrawer();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        dbAdapter = new DBAdapter(context);
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bin, container, false);
        toolbar = view.findViewById(R.id.toolbar_custom);
        ic_hamberger = view.findViewById(R.id.ic_hamberger);
        rvNoteList = view.findViewById(R.id.rvNotesList);
        rlRootLayout = view.findViewById(R.id.rlRootLayout);
        ic_light = view.findViewById(R.id.ic_light);
        etSearch =view.findViewById(R.id.etSearch);
        borderEditText = view.findViewById(R.id.borderEditText);
        llnoNotes = view.findViewById(R.id.llNoNotes);
        binNoteList =dbAdapter.getAllNotesFromBin();
        tvEmptyList = view.findViewById(R.id.tvEmptyList);
        if(binNoteList.size() ==0)
        {
            rvNoteList.setVisibility(View.GONE);
            llnoNotes.setVisibility(View.VISIBLE);
        }
        else
        {
            rvNoteList.setVisibility(View.VISIBLE);
            llnoNotes.setVisibility(View.GONE);
        }
        setAdapter();
//        attachItemTouchListeners();
        setClickListeners();
        initSharedPref();
        isDark = sharedPreferences.getBoolean(Constants.IS_LIGHT_THEME,false);
        switchTheme();
        return view;
    }

    public void setAdapter()
    {
        noteAdapter =new NoteAdapter(context,binNoteList,this);
        rvNoteList.setLayoutManager(new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL));
        rvNoteList.setAdapter(noteAdapter);

    }

    @Override
    public void rowItemClicked(View v, int position) {
        Note note;
        if(isInSearchMode)
        {
            note = filteredData.get(position);
        }
        else
        {
            note = binNoteList.get(position);
        }

        startBin_Activity(note);
    }

    @Override
    public void requestDrag(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    public void checkBoxClicked(View v, int position) {

    }

    private void startBin_Activity(Note note)
    {
        Intent intent = new Intent(getContext(), Bin_Activity.class);

            intent.putExtra("NoteObject", note);

        startActivityForResult(intent, 2);
    }

    public void setClickListeners()
    {
        ic_hamberger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });

        ic_light.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchTheme();
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filter(etSearch.getText().toString());
                isInSearchMode = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void filter(String text)
    {
        if(text.trim().equals(""))
        {
            filteredData = binNoteList;
            SimpleItemTouchHelperCallback.draggable = true;
            noteAdapter.updateAdapter(filteredData);
            return;
        }
        else
        {
            SimpleItemTouchHelperCallback.draggable = false;
        }

        List<Note> filteredList = new ArrayList<Note>();

        for(Note note : binNoteList)
        {
            if(note.getTitle() != null && note.getContent() != null)
            {
                if(note.getTitle().toLowerCase().contains(text.trim().toLowerCase()) || note.getContent().toLowerCase().contains(text.trim().toLowerCase()))
                {
                    filteredList.add(note);
                }
            }

        }

        filteredData = filteredList;
        noteAdapter.updateAdapter(filteredData);
    }

    @Override
    public void onResume() {
        binNoteList = dbAdapter.getAllNotesFromBin();
        noteAdapter.updateAdapter(binNoteList);
        isInSearchMode = false;
        if(binNoteList.size() ==0)
        {
            rvNoteList.setVisibility(View.GONE);
            llnoNotes.setVisibility(View.VISIBLE);
        }
        else
        {
            rvNoteList.setVisibility(View.VISIBLE);
            llnoNotes.setVisibility(View.GONE);
        }
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 2) {
            if(resultCode == Activity.RESULT_OK){

                switch (data.getStringExtra(Constants.ACTION_NOTE))
                {
                    case Constants.IS_RESTORED_NOTE:
                    {
                        Snackbar.make(drawer, "Note restored.", Snackbar.LENGTH_LONG).show();
                        break;
                    }
                    case Constants.IS_PERMANENT_DELETED_NOTE:
                    {
                        Snackbar.make(drawer, "Note deleted.", Snackbar.LENGTH_LONG).show();
                        break;
                    }


                    default:
                    {
                        break;
                    }

                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }


    private void switchTheme()
    {
        if(isDark)
        {
            ic_hamberger.setImageResource(R.drawable.ic_hamberger_dark);
            ic_light.setImageResource(R.drawable.ic_bulb_dark);
            rlRootLayout.setBackgroundColor(getResources().getColor(R.color.color_Cream));
            etSearch.setTextColor(getResources().getColor(R.color.color_Black));
            etSearch.setHintTextColor(getResources().getColor(R.color.color_Grey_dark));
            borderEditText.setBackground((getResources().getDrawable(R.drawable.border_chamfer_dark)));
            statusBarColor(getString(R.string.color_Cream));
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            tvEmptyList.setTextColor(getResources().getColor(R.color.color_Black));
            isDark = false;
            editor.putBoolean(Constants.IS_LIGHT_THEME,true);
            editor.apply();
        }
        else
        {
            ic_hamberger.setImageResource(R.drawable.ic_hamberger_light);
            ic_light.setImageResource(R.drawable.ic_bulb_light);
            rlRootLayout.setBackgroundColor(getResources().getColor(R.color.color_Black));
            etSearch.setTextColor(getResources().getColor(R.color.color_White));
            etSearch.setHintTextColor(getResources().getColor(R.color.color_Cream));
            borderEditText.setBackground((getResources().getDrawable(R.drawable.border_chamfer_light)));
            statusBarColor(getString(R.string.color_Black));
            View decorView = getActivity().getWindow().getDecorView(); //set status background black
            decorView.setSystemUiVisibility(decorView.getSystemUiVisibility() & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            tvEmptyList.setTextColor(getResources().getColor(R.color.color_Cream));
            isDark = true;
            editor.putBoolean(Constants.IS_LIGHT_THEME,false);
            editor.apply();
        }

    }

    private void initSharedPref()
    {
        sharedPreferences = getActivity().getSharedPreferences(Constants.THEME_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void statusBarColor(String colorCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(colorCode));
        }
    }
}