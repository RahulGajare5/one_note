package com.rg.one_note.Utility;

import android.content.Context;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utilities {

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd-MM-yyyy HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }


    public static boolean parseIntToBoolean(int value)
    {
        if(value == 0)
        {
            return  false;
        }

        return  true;
    }

    public static int parseBooleanToInt(boolean value)
    {
        if(value)
        {
            return 1;
        }

        return 0;

    }

    public static void showToast(Context context, String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG ).show();
    }

}
