package com.rg.one_note.Drawer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.stetho.Stetho;
import com.google.android.material.navigation.NavigationView;
import com.rg.one_note.Interfaces.ActivityCommunication;
import com.rg.one_note.R;
import com.rg.one_note.fragments.BinFragment;
import com.rg.one_note.fragments.NoteFragment;
import com.rg.one_note.fragments.SettingFragment;
import com.rg.one_note.fragments.TaskFragment;

public class Drawer_Activity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ActivityCommunication {

    private DrawerLayout drawer;
    private Toolbar toolbar;
    SQLiteDatabase mDatabase;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private Drawer_Activity drawer_activity =this;
    private EditText editText;
    private TextView nav_header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Stetho.initializeWithDefaults(this);

        drawer = findViewById(R.id.drawer_layout);
        nav_header = findViewById(R.id.nav_header);


        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        attachFragment();

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this != null && this.getWindow() != null && this.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(this.getWindow().getDecorView().getWindowToken(), 0);
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public DrawerLayout getDrawer()
    {
        return drawer;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_notes: {
                NoteFragment noteFragment = new NoteFragment(this);
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentContainer,noteFragment);
                fragmentTransaction.commit();
                break;
            }
            case R.id.nav_tasks: {
                TaskFragment taskFragment = new TaskFragment(this);
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentContainer,taskFragment);
                fragmentTransaction.commit();
                break;
            }

            case R.id.nav_settings: {
                SettingFragment settingFragmentFragment = new SettingFragment(this);
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentContainer,settingFragmentFragment);
                fragmentTransaction.commit();
                break;
            }

            case R.id.nav_bin:
            {
                BinFragment binFragment = new BinFragment(this);
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentContainer,binFragment);
                fragmentTransaction.commit();
                break;
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }

    public void attachFragment()
    {
        NoteFragment noteFragment = new NoteFragment(this);
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fragmentContainer,noteFragment);
        fragmentTransaction.commit();

    }

}
