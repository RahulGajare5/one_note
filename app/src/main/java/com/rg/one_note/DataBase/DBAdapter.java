package com.rg.one_note.DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.rg.one_note.Constants.Constants;
import com.rg.one_note.Utility.Utilities;

import java.util.ArrayList;
import java.util.List;

import Models.Note;
import Models.Task;

public class DBAdapter {

    private DBHelper dbHelper;
    private SQLiteDatabase db;

    public DBAdapter(Context context)
    {
         dbHelper = new DBHelper(context,Constants.DATA_BASE_NAME,null,Constants.DATA_BASE_VERSION);
       db = dbHelper.getWritableDatabase();
    }

    public long insertNote(Note note)
    {

        try {
//            SQLiteDatabase db= dbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(Constants.NOTE_TITLE,note.getTitle());
            contentValues.put(Constants.NOTE_CONTENT,note.getContent());
            contentValues.put(Constants.NOTE_DATE_STAMP,note.getDateStamp().toString());
            contentValues.put(Constants.NOTE_COLOR,note.getNoteColor());
            contentValues.put(Constants.TEXT_COLOR,note.getTextColor());
            contentValues.put(Constants.NOTE_IS_PINNED,Utilities.parseBooleanToInt(note.getIsPinned()));
            contentValues.put(Constants.NOTE_META_DATA,note.getMetadata());

            long row = db.insert(Constants.NAME_NOTES_TABLE,null,contentValues);

            return  row;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public long insertNoteInBin(Note note)
    {

        try {
//            SQLiteDatabase db= dbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(Constants.NOTE_TITLE,note.getTitle());
            contentValues.put(Constants.NOTE_CONTENT,note.getContent());
            contentValues.put(Constants.NOTE_DATE_STAMP,note.getDateStamp().toString());
            contentValues.put(Constants.NOTE_COLOR,note.getNoteColor());
            contentValues.put(Constants.TEXT_COLOR,note.getTextColor());
            contentValues.put(Constants.NOTE_IS_PINNED,Utilities.parseBooleanToInt(note.getIsPinned()));
            contentValues.put(Constants.NOTE_META_DATA,note.getMetadata());

            long row = db.insert(Constants.NAME_BIN_NOTES_TABLE,null,contentValues);

            return  row;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }


    public void addAllDataFromListToBInNoteTable(List<Note> list)
    {
        for(Note note : list)
        {
            insertNoteInBin(note);
        }

    }

    public  List<Note> getAllNotes()
    {
        try
        {
//            SQLiteDatabase db= dbHelper.getWritableDatabase();
            String [] columns = {Constants.NOTE_ID,Constants.NOTE_TITLE,Constants.NOTE_CONTENT,Constants.NOTE_DATE_STAMP,Constants.NOTE_COLOR,Constants.TEXT_COLOR,Constants.NOTE_IS_PINNED,Constants.NOTE_DRAGGED_POSITION};
            Cursor cusrsor = db.query(Constants.NAME_NOTES_TABLE,columns,null,null,null,null,Constants.NOTE_DRAGGED_POSITION);
            return createNotesFromCursor(cusrsor);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public  List<Note> getAllNotesFromBin()
    {
        try
        {
//            SQLiteDatabase db= dbHelper.getWritableDatabase();
            String [] columns = {Constants.NOTE_ID,Constants.NOTE_TITLE,Constants.NOTE_CONTENT,Constants.NOTE_DATE_STAMP,Constants.NOTE_COLOR,Constants.TEXT_COLOR,Constants.NOTE_IS_PINNED,Constants.NOTE_DRAGGED_POSITION};
            Cursor cusrsor = db.query(Constants.NAME_BIN_NOTES_TABLE,columns,null,null,null,null,Constants.NOTE_ID + " DESC");
            return createNotesFromCursor(cusrsor);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public long updateNote(Note note)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.NOTE_TITLE,note.getTitle());
        contentValues.put(Constants.NOTE_CONTENT,note.getContent());
        contentValues.put(Constants.NOTE_DATE_STAMP,note.getDateStamp().toString());
        contentValues.put(Constants.NOTE_COLOR,note.getNoteColor());
        contentValues.put(Constants.TEXT_COLOR,note.getTextColor());
        contentValues.put(Constants.NOTE_IS_PINNED,Utilities.parseBooleanToInt(note.getIsPinned()));
        contentValues.put(Constants.NOTE_META_DATA,note.getMetadata());
        long row = db.update(Constants.NAME_NOTES_TABLE, contentValues, Constants.NOTE_ID + "=?",new String[]{""+note.getNote_id()} );
        return row;
    }


    public long addDraggedPosition(Note note,long position)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.NOTE_DRAGGED_POSITION,position);
        long row = db.update(Constants.NAME_NOTES_TABLE, contentValues, Constants.NOTE_ID + "=?",new String[]{""+note.getNote_id()} );
        return row;

    }


    public void clearDataFromNoteTable()
    {
        db.execSQL("DELETE FROM " + Constants.NAME_NOTES_TABLE);
    }

    public void clearDataFromBinTable() {
        db.execSQL("DELETE FROM " + Constants.NAME_BIN_NOTES_TABLE);
    }

    public long deleteNote(Note note)
    {
        long row = db.delete(Constants.NAME_NOTES_TABLE, Constants.NOTE_ID + "=?",new String[]{""+note.getNote_id()} );

        return row;
    }

    public long deleteNoteFromBin(Note note)
    {
        long row = db.delete(Constants.NAME_BIN_NOTES_TABLE, Constants.NOTE_ID + "=?",new String[]{""+note.getNote_id()} );

        return row;
    }


    public List<Note> createNotesFromCursor(Cursor cursor)
    {
        List<Note> noteList = new ArrayList<Note>();
        int index_id = cursor.getColumnIndex(Constants.NOTE_ID);
        int index_title = cursor.getColumnIndex(Constants.NOTE_TITLE);
        int index_content = cursor.getColumnIndex(Constants.NOTE_CONTENT);
        int index_dateStamp = cursor.getColumnIndex(Constants.NOTE_DATE_STAMP);
        int index_noteColor = cursor.getColumnIndex(Constants.NOTE_COLOR);
        int index_textColor = cursor.getColumnIndex(Constants.TEXT_COLOR);
        int index_isPinned = cursor.getColumnIndex(Constants.NOTE_IS_PINNED);
        int index_draggedPosition = cursor.getColumnIndex(Constants.NOTE_DRAGGED_POSITION);

        while(cursor.moveToNext())
        {
            Long note_id = cursor.getLong(index_id);
            String title = cursor.getString(index_title);
            String content = cursor.getString(index_content);
            String dateStamp = cursor.getString(index_dateStamp);
            String noteColor = cursor.getString(index_noteColor);
            String textColor = cursor.getString(index_textColor);
            int isPinned = cursor.getInt(index_isPinned);
            long draggedPosition = cursor.getLong(index_draggedPosition);

            Note note = new Note();
            note.setNote_id(note_id);
            note.setTitle(title);
            note.setContent(content);
            note.setDateStamp(dateStamp);
            note.setNoteColor(noteColor);
            note.setTextColor(textColor);
            note.setPinned(Utilities.parseIntToBoolean(isPinned));
            note.setDraggedPosition(draggedPosition);

            noteList.add(note);


        }

        return noteList;

    }

    public long insertTask(Task task)
    {

        try {
//            SQLiteDatabase db= dbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(Constants.TASK_CONTENT,task.getContent());
            contentValues.put(Constants.TASK_DETAILS,task.getDetails());
            contentValues.put(Constants.TASK_ISCOMPLETED,task.getIsCompleted());
            contentValues.put(Constants.TASK_DATE_STAMP,task.getCreatedDate());
            contentValues.put(Constants.TASK_REMINDER_DATE_TIME,task.getReminderDate_Time());

            long row = db.insert(Constants.NAME_TASK_TABLE,null,contentValues);

            return  row;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public long insertTaskInCompleted(Task task)
    {

        try {
//            SQLiteDatabase db= dbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(Constants.TASK_CONTENT,task.getContent());
            contentValues.put(Constants.TASK_DETAILS,task.getDetails());
            contentValues.put(Constants.TASK_ISCOMPLETED,task.getIsCompleted());
            contentValues.put(Constants.TASK_DATE_STAMP,task.getCreatedDate());
            contentValues.put(Constants.TASK_REMINDER_DATE_TIME,task.getReminderDate_Time());

            long row = db.insert(Constants.NAME_TASK_COMPLETED_TABLE,null,contentValues);

            return  row;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public  List<Task> getAllTask()
    {
        try
        {
//            SQLiteDatabase db= dbHelper.getWritableDatabase();
            String [] columns = {Constants.TASK_ID,Constants.TASK_CONTENT,Constants.TASK_DETAILS,Constants.TASK_DATE_STAMP,Constants.TASK_REMINDER_DATE_TIME,Constants.TASK_ISCOMPLETED,Constants.TASK_DRAGGED_POSITION};
            Cursor cusrsor = db.query(Constants.NAME_TASK_TABLE,columns,null,null,null,null,Constants.TASK_DRAGGED_POSITION);
            return createTaskFromCursor(cusrsor);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }

    public  List<Task> getAllTaskFromCompleted()
    {
        try
        {
//            SQLiteDatabase db= dbHelper.getWritableDatabase();
            String [] columns = {Constants.TASK_ID,Constants.TASK_CONTENT,Constants.TASK_DETAILS,Constants.TASK_DATE_STAMP,Constants.TASK_REMINDER_DATE_TIME,Constants.TASK_ISCOMPLETED,Constants.TASK_DRAGGED_POSITION};
            Cursor cusrsor = db.query(Constants.NAME_TASK_COMPLETED_TABLE,columns,null,null,null,null,Constants.TASK_ID + " DESC");
            return createTaskFromCursor(cusrsor);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            return null;
        }


    }
    public void clearDataFromCompletedTask() {
        db.execSQL("DELETE FROM " + Constants.NAME_TASK_COMPLETED_TABLE);
    }


    public List<Task> createTaskFromCursor(Cursor cursor)
    {
        List<Task> taskList = new ArrayList<Task>();
        int index_id = cursor.getColumnIndex(Constants.TASK_ID);
        int index_content = cursor.getColumnIndex(Constants.TASK_CONTENT);
        int index_details = cursor.getColumnIndex(Constants.TASK_DETAILS);
        int index_dateStamp = cursor.getColumnIndex(Constants.TASK_DATE_STAMP);
        int index_reminderDate = cursor.getColumnIndex(Constants.TASK_REMINDER_DATE_TIME);
        int index_isCompleted = cursor.getColumnIndex(Constants.TASK_ISCOMPLETED);
        int index_draggedPosition = cursor.getColumnIndex(Constants.TASK_DRAGGED_POSITION);

        while(cursor.moveToNext())
        {
            Long task_id = cursor.getLong(index_id);
            String content = cursor.getString(index_content);
            String details = cursor.getString(index_details);
            String dateStamp = cursor.getString(index_dateStamp);
            String reminderDate = cursor.getString(index_reminderDate);
            int isCompleted = cursor.getInt(index_isCompleted);
            long draggedPosition = cursor.getLong(index_draggedPosition);

            Task task = new Task();
            task.setTask_id(task_id);
            task.setContent(content);
            task.setDetails(details);
            task.setIsCompleted(isCompleted);
            task.setDraggedPosition(draggedPosition);
            task.setCreatedDate(dateStamp);
            task.setReminderDate_Time(reminderDate);

            taskList.add(task);

        }
        return taskList;
    }

    public long deleteTask(Task task)
    {
        long row = db.delete(Constants.NAME_TASK_TABLE, Constants.TASK_ID + "=?",new String[]{""+task.getTask_id()} );

        return row;
    }

    public long deleteTaskFromCompleted(Task task)
    {
        long row = db.delete(Constants.NAME_TASK_COMPLETED_TABLE, Constants.TASK_ID + "=?",new String[]{""+task.getTask_id()} );

        return row;
    }

    public long updateTask(Task task)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.TASK_CONTENT,task.getContent());
        contentValues.put(Constants.TASK_DATE_STAMP,task.getCreatedDate());
        long row = db.update(Constants.NAME_TASK_TABLE, contentValues, Constants.TASK_ID + "=?",new String[]{""+task.getTask_id()} );
        return row;
    }

    public Task getTaskById(long task_id)
    {
        String selectQuery = "SELECT  * FROM " + Constants.NAME_TASK_TABLE + " WHERE "
                + Constants.TASK_ID + " = " + task_id;
       Cursor cursor = db.rawQuery(selectQuery,null);
       List<Task> list = createTaskFromCursor(cursor);
       return list.get(0);
    }


    public long addDraggedPosition(Task task,long position)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.TASK_DRAGGED_POSITION,position);
        long row = db.update(Constants.NAME_TASK_TABLE, contentValues, Constants.NOTE_ID + "=?",new String[]{""+task.getTask_id()} );
        return row;

    }


    public static class DBHelper extends SQLiteOpenHelper {

        private Context context ;
        private static final String noteTabelQuery = "CREATE TABLE IF NOT EXISTS " + Constants.NAME_NOTES_TABLE
                + "("
                + Constants.NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Constants.NOTE_TITLE + " TEXT NOT NULL, "
                + Constants.NOTE_CONTENT + " TEXT NOT NULL, "
                + Constants.NOTE_DATE_STAMP + " TEXT NOT NULL, "
                + Constants.NOTE_COLOR + " TEXT NOT NULL, "
                + Constants.TEXT_COLOR + " TEXT NOT NULL, "
                + Constants.NOTE_META_DATA + " TEXT NOT NULL, "
                + Constants.NOTE_IS_PINNED + " INTEGER DEFAULT 0, "
                + Constants.NOTE_DRAGGED_POSITION + " INTEGER DEFAULT 0 "
                + ");";

        private static final String binNoteTabelQuery = "CREATE TABLE IF NOT EXISTS " + Constants.NAME_BIN_NOTES_TABLE
                + "("
                + Constants.NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Constants.NOTE_TITLE + " TEXT NOT NULL, "
                + Constants.NOTE_CONTENT + " TEXT NOT NULL, "
                + Constants.NOTE_DATE_STAMP + " TEXT NOT NULL, "
                + Constants.NOTE_COLOR + " TEXT NOT NULL, "
                + Constants.TEXT_COLOR + " TEXT NOT NULL, "
                + Constants.NOTE_META_DATA + " TEXT NOT NULL, "
                + Constants.NOTE_IS_PINNED + " INTEGER DEFAULT 0, "
                + Constants.NOTE_DRAGGED_POSITION + " INTEGER DEFAULT 0 "
                + ");";

        private static final String taskTableQuery = "CREATE TABLE IF NOT EXISTS " + Constants.NAME_TASK_TABLE
                + "("
                + Constants.TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Constants.TASK_CONTENT + " TEXT NOT NULL, "
                + Constants.TASK_ISCOMPLETED + " INTEGER NOT NULL, "
                + Constants.TASK_DATE_STAMP + " TEXT NOT NULL, "
                + Constants.TASK_DETAILS + " TEXT NOT NULL, "
                + Constants.TASK_REMINDER_DATE_TIME + " TEXT NOT NULL, "
                + Constants.NOTE_DRAGGED_POSITION + " INTEGER DEFAULT 0 "
                + ");";

        private static final String taskCompletedTableQuery = "CREATE TABLE IF NOT EXISTS " + Constants.NAME_TASK_COMPLETED_TABLE
                + "("
                + Constants.TASK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Constants.TASK_CONTENT + " TEXT NOT NULL, "
                + Constants.TASK_DETAILS + " TEXT NOT NULL, "
                + Constants.TASK_ISCOMPLETED + " INTEGER NOT NULL, "
                + Constants.TASK_DATE_STAMP + " TEXT NOT NULL, "
                + Constants.TASK_REMINDER_DATE_TIME + " TEXT NOT NULL, "
                + Constants.NOTE_DRAGGED_POSITION + " INTEGER DEFAULT 0 "
                + ");";




        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
            this.context =context;
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {

            try {
                sqLiteDatabase.execSQL(noteTabelQuery);
                sqLiteDatabase.execSQL(binNoteTabelQuery);
                sqLiteDatabase.execSQL(taskTableQuery);
                sqLiteDatabase.execSQL(taskCompletedTableQuery);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }
    }
}
