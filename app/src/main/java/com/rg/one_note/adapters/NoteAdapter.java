package com.rg.one_note.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.one_note.Interfaces.ItemTouchHelperAdapter;
import com.rg.one_note.Interfaces.RecyclerViewClickListener;
import com.rg.one_note.R;
import com.rg.one_note.fragments.NoteFragment;

import java.util.Collections;
import java.util.List;

import Models.Note;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> implements ItemTouchHelperAdapter {

    private List<Note> notesList;
    private Context context;
    private final LayoutInflater inflater;
    private RecyclerViewClickListener recyclerViewClickListener;
    private long lastClickTime = 0;

    public NoteAdapter(Context context, List<Note> notesList,RecyclerViewClickListener recyclerViewClickListener)
    {
        this.context = context;
        this.notesList = notesList;
        this.inflater = LayoutInflater.from(context);
        this.recyclerViewClickListener = recyclerViewClickListener;

    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.note_item, parent,false);
        NoteViewHolder noteViewHolder = new NoteViewHolder(view);
        return noteViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        Note note = notesList.get(position);
        holder.tvTitle.setText(note.getTitle());
        holder.tvContent.setText(note.getContent());
        holder.tvDateStamp.setText(note.getDateStamp());
        holder.llnoteContainer.setBackgroundColor(Color.parseColor(note.getNoteColor()));
        holder.tvContent.setTextColor(Color.parseColor(note.getTextColor()));
    }

    @Override
    public int getItemCount() {
        return notesList.size();
    }


    public void updateAdapter(List<Note> list)
    {
        this.notesList = list;
        notifyDataSetChanged();

    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(notesList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(notesList, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {

            notifyItemRemoved(position);
    }




    public class NoteViewHolder extends RecyclerView.ViewHolder
    {
        private TextView tvTitle,tvContent,tvDateStamp;
        private LinearLayout llnoteContainer;

        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvContent = itemView.findViewById(R.id.tvContent);
            tvDateStamp = itemView.findViewById(R.id.tvDateStamp);
            llnoteContainer = itemView.findViewById(R.id.llnoteContainer);

            llnoteContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
                        return;
                    }

                    lastClickTime = SystemClock.elapsedRealtime();
                    recyclerViewClickListener.rowItemClicked(view,getLayoutPosition());
                }
            });
        }
    }
}
