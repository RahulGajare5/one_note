package com.rg.one_note.adapters;

import android.content.Context;
import android.graphics.Color;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.one_note.Interfaces.ItemTouchHelperAdapter;
import com.rg.one_note.Interfaces.RecyclerViewClickListener;
import com.rg.one_note.R;

import java.util.Collections;
import java.util.List;

import Models.Task;

public class TaskAdapter  extends RecyclerView.Adapter<TaskAdapter.TaskViewHolder> implements ItemTouchHelperAdapter {
    private List<Task> taskList;
    private Context context;
    private final LayoutInflater inflater;
    private RecyclerViewClickListener recyclerViewClickListener;
    private long lastClickTime = 0;
    private boolean isCompletedTask;
    private boolean isInSearchMode;

    public TaskAdapter(Context context, List<Task> taskList, RecyclerViewClickListener recyclerViewClickListener)
    {
        this.context = context;
        this.taskList = taskList;
        this.inflater = LayoutInflater.from(context);
        this.recyclerViewClickListener = recyclerViewClickListener;
    }


    @NonNull
    @Override
    public TaskAdapter.TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.task_item, parent,false);
        TaskViewHolder taskViewHolder = new TaskViewHolder(view);
        return taskViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final TaskAdapter.TaskViewHolder holder, int position) {
        Task task = taskList.get(position);



        if(isCompletedTask)
        {
            holder.checkBox.setChecked(true);
            holder.ic_drag.setVisibility(View.GONE);
        }
        else
        {
            holder.checkBox.setChecked(false);
            holder.ic_drag.setVisibility(View.VISIBLE);
        }

        if(isInSearchMode && !isCompletedTask)
        {
            holder.ic_drag.setVisibility(View.GONE);
        }

        holder.tvContent.setText(task.getContent());
        holder.ic_drag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() ==
                        MotionEvent.ACTION_DOWN) {
                    recyclerViewClickListener.requestDrag(holder);
                }
                return false;
            }

        });
    }

    @Override
    public int getItemCount() {
        return this.taskList.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(taskList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(taskList, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {

        notifyItemRemoved(position);
    }

    public void updateAdapter(List<Task> list,boolean isCompletedTask,boolean isInSearchMode)
    {
        this.taskList = list;
        notifyDataSetChanged();
        this.isCompletedTask = isCompletedTask;
        this.isInSearchMode = isInSearchMode;

    }

    public class TaskViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tvContent;
        private CheckBox checkBox;
        private ImageView ic_drag;
        public TaskViewHolder(@NonNull View itemView) {
            super(itemView);

            tvContent = itemView.findViewById(R.id.tvContent);
            checkBox = itemView.findViewById(R.id.checkBox);
            ic_drag = itemView.findViewById(R.id.ic_drag);
            tvContent.setOnClickListener(this);
            checkBox.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            switch(view.getId())
            {
                case R.id.checkBox:
                {
                    recyclerViewClickListener.checkBoxClicked(view,getLayoutPosition());
                     break;
                }

                case R.id.tvContent:
                {
                    if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
                        return;
                    }
                    lastClickTime = SystemClock.elapsedRealtime();
                    recyclerViewClickListener.rowItemClicked(view,getLayoutPosition());
                    break;
                }

                default:
                {
                    break;
                }
            }
        }
    }
}
