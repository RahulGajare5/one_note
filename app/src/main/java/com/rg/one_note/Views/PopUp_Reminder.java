package com.rg.one_note.Views;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.rg.one_note.Interfaces.IReminderPopupListener;
import com.rg.one_note.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class PopUp_Reminder implements View.OnClickListener {

    private TextInputEditText etDate,etTime;
    private TextInputLayout timeInputlayout;
    private Context context;
    private  final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog mTimePicker;
    private IReminderPopupListener iReminderPopupListener;
    private Button btnSave;
    private Date dateFromCalandar;
    private long duration;
    private  PopupWindow popupWindow;
    private boolean isReminderTimeValid = false;


    public void showPopupWindow(final View view, Context context,IReminderPopupListener iReminderPopupListener)
    {
        this.context = context;
        this.iReminderPopupListener = iReminderPopupListener;
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.reminder_popup, null);
        initViews(popupView);
        setClickListeners();
        setDateText();



//Specify the length and width through constants
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        popupWindow = new PopupWindow(popupView, width, height, focusable);

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Handler for clicking on the inactive zone of the window

//        popupView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                //Close the window when clicked
//                popupWindow.dismiss();
//                return true;
//            }
//        });
    }

    private void initViews(View popupView)
    {
        etDate =  popupView.findViewById(R.id.etDate);
        etTime = popupView.findViewById(R.id.etTime);
        btnSave = popupView.findViewById(R.id.btnsave);
        timeInputlayout = popupView.findViewById(R.id.timeInputlayout);
    }

    private void setClickListeners()
    {
        etDate.setOnClickListener(this);
        etTime.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                setDateText();
            }


        };
    }

    private void showDatePicker()
    {

        datePickerDialog =   new DatePickerDialog(context, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
        long now = myCalendar.getTimeInMillis();
        datePickerDialog.getDatePicker().setMinDate(now);


    }

    private void showTimePicker()
    {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        mTimePicker = new TimePickerDialog(this.context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
               setTimeText(selectedHour,selectedMinute);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }


    private void setDateText() {
        String myFormat = "dd/MM/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        dateFromCalandar = myCalendar.getTime();

        etDate.setText(sdf.format(myCalendar.getTime()));

    }

    private  void setTimeText(int selectedHour , int selectedMinute)
    {

        dateFromCalandar.setHours(selectedHour);
        dateFromCalandar.setMinutes(selectedMinute);
         duration = dateFromCalandar.getTime() - new Date().getTime();

         String time = "";
        if(selectedHour>=0 && selectedHour<12){
            time = selectedHour + " : " + selectedMinute + " AM";
        } else {
            if(selectedHour == 12){
                time = selectedHour + " : " + selectedMinute + " PM";
            } else{
                selectedHour = selectedHour -12;
                time = selectedHour + " : " + selectedMinute + " PM";
            }
        }

        etTime.setText(time);

//        String timeSet = "";
//        if (selectedHour > 12) {
//            selectedHour -= 12;
//            timeSet = "PM";
//        } else if (selectedHour == 0) {
//            selectedHour += 12;
//            timeSet = "AM";
//        } else if (selectedHour == 12)
//            timeSet = "PM";
//        else
//            timeSet = "AM";
//
//
//        String minutes = "";
//        if (selectedHour < 10)
//            minutes = "0" + selectedMinute;
//        else
//            minutes = String.valueOf(selectedMinute);
//
//        // Append in a StringBuilder
//        String aTime = new StringBuilder().append(selectedHour).append(':')
//                .append(minutes).append(" ").append(timeSet).toString();
//        etTime.setText(aTime);

        if(duration <= 0) {
            timeInputlayout.setError("Time must be from future.");
            isReminderTimeValid = false;
            return;
        }
        if(timeInputlayout.isErrorEnabled()){
            timeInputlayout.setErrorEnabled(false);
        }
        isReminderTimeValid = true;

    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.etDate:
            {
                showDatePicker();
                break;
            }
            case R.id.etTime:
            {
                showTimePicker();
                break;
            }
            case R.id.btnsave:
            {
                if(isReminderTimeValid)
                {
                    iReminderPopupListener.DateAndTimeFromPopUp(duration);
                    popupWindow.dismiss();


                }
                break;
            }

            default:{
                break;
            }
        }
    }
}

