package com.rg.one_note.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.material.snackbar.Snackbar;
import com.rg.one_note.Constants.Constants;
import com.rg.one_note.DataBase.DBAdapter;
import com.rg.one_note.R;
import com.rg.one_note.fragments.BinFragment;
import com.rg.one_note.fragments.NoteFragment;

import java.util.List;

import Models.Note;

public class Bin_Activity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout rlRootLayout;
    private EditText etTitle, etContent;
    private DBAdapter dbAdapter;
    private Note note;
    private ImageView ivRestore, ic_delete , ivGoback ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bin_);

        dbAdapter = new DBAdapter(this);

        initViews();
        diableEditing();
        setClickListenrs();

        Intent intent =getIntent();
        note = (Note)intent.getSerializableExtra("NoteObject");
        if(note != null)
            initValuesForViews();
        statusBarColor(note.getNoteColor());
    }

    private void initViews()
    {
        etTitle = findViewById(R.id.etTitle);
        etContent = findViewById(R.id.etContent);
        ivRestore = findViewById(R.id.ic_restore);
        rlRootLayout = findViewById(R.id.rlRootLayout);
        ic_delete = findViewById(R.id.ic_delete);
        ivGoback = findViewById(R.id.ic_goback);

    }

    private void diableEditing()
    {
        etTitle.setKeyListener(null);
        etContent.setKeyListener(null);
    }

    public void statusBarColor(String colorCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(colorCode));
        }
    }

    private void setClickListenrs()
    {

        ivRestore.setOnClickListener(this);
        ic_delete.setOnClickListener(this);
        ivGoback.setOnClickListener(this);
        etTitle.setOnClickListener(this);
        etContent.setOnClickListener(this);

    }
    private void initValuesForViews()
    {
        etTitle.setText(note.getTitle());
        etContent.setText(note.getContent());
        rlRootLayout.setBackgroundColor(Color.parseColor(note.getNoteColor()));
        etContent.setTextColor(Color.parseColor(note.getTextColor()));
    }


    public void restoreFromBin()
    {
        dbAdapter.deleteNoteFromBin(note);
        note.setNote_id(null);
        long row =dbAdapter.insertNote(note);
        note.setNote_id(row);
        NoteFragment.noteList.add(0,note);
        for(Note note :  NoteFragment.noteList)
        {
            long position = getindexByproperty(note.getNote_id());
            dbAdapter.addDraggedPosition(note,position);
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.ACTION_NOTE,Constants.IS_RESTORED_NOTE);
        setResult(this.RESULT_OK,returnIntent);
        finish();

    }

    public void showDialog()
    {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this,R.style.MyDialogTheme);
        builder1.setMessage("Do you want to delete this note permanently?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        deletePermanently();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }

    private void deletePermanently()
    {

        long row =  dbAdapter.deleteNoteFromBin(note);

        if(row > -1)
        {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.ACTION_NOTE,Constants.IS_PERMANENT_DELETED_NOTE);
            setResult(this.RESULT_OK,returnIntent);
            finish();
        }
    }

    @Override
    protected void onPause() {
        for(Note note : NoteFragment.noteList)
        {
            long position = getindexByproperty(note.getNote_id());
            dbAdapter.addDraggedPosition(note,position);
        }
        super.onPause();
    }

    private long getindexByproperty(long noteID) {
        for (int i = 0; i < NoteFragment.noteList.size(); i++) {
            if (NoteFragment.noteList.get(i).getNote_id() == noteID) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onClick(View view) {

        switch(view.getId())
        {
            case R.id.ic_restore :
            {
                restoreFromBin();
                break;
            }

            case R.id.ic_delete:
            {
                showDialog();
                break;
            }
            case R.id.ic_goback:
            {
                onBackPressed();
                break;
            }
            case R.id.etTitle:
            {
                Snackbar.make(rlRootLayout, "Restore the note to edit.", Snackbar.LENGTH_LONG).show();
                break;
            }
            case R.id.etContent:
            {
                Snackbar.make(rlRootLayout, "Restore the note to edit.", Snackbar.LENGTH_LONG).show();
                break;
            }
            default:
            {
                break;
            }
        }
    }
}