package com.rg.one_note.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.biometrics.BiometricPrompt;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.rg.one_note.Constants.Constants;
import com.rg.one_note.Drawer.Drawer_Activity;
import com.rg.one_note.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private CancellationSignal cancellationSignal;
    private Button btnAuthenticate;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private  boolean isFingerprintEnabled;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        statusBarColor(getString(R.string.color_Cream));

        btnAuthenticate = findViewById(R.id.btnAuthenticate);
        btnAuthenticate.setOnClickListener(this);
        initSharedPref();
        isFingerprintEnabled = sharedPreferences.getBoolean(Constants.ISFRINGERPRINTENABLED,false);
        if(isFingerprintEnabled)
        {
            if(checkBiometricSupport())
            {
                authenticateUser();
            }
        }
        else
        {
            startDrawerActivity();
        }

    }
    private boolean isSdkVersionSupported() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }


        private void notifyUser(String message) {
        Toast.makeText(this,
                message,
                Toast.LENGTH_LONG).show();
    }

    private Boolean checkBiometricSupport() {

        KeyguardManager keyguardManager =
                (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

        PackageManager packageManager = this.getPackageManager();

        if (!keyguardManager.isKeyguardSecure()) {
            notifyUser("Lock screen security not enabled in Settings");
            return false;
        }

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.USE_BIOMETRIC) !=
                PackageManager.PERMISSION_GRANTED) {

            notifyUser("Fingerprint authentication permission not enabled");
            return false;
        }

        if (packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT))
        {
            return true;
        }

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    private BiometricPrompt.AuthenticationCallback getAuthenticationCallback() {

        return new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              CharSequence errString) {
                if(BiometricPrompt.BIOMETRIC_ERROR_NO_BIOMETRICS == errorCode)
                {

                    isFingerprintEnabled = false;
                    editor.putBoolean(Constants.ISFRINGERPRINTENABLED,false);
                    editor.apply();
                    startDrawerActivity();
                }


                if(BiometricPrompt.BIOMETRIC_ERROR_USER_CANCELED == errorCode) {

                }

                if(BiometricPrompt.BIOMETRIC_ERROR_LOCKOUT == errorCode)
                {
                    notifyUser("Too many wrong attempts, Try later");
                }
                super.onAuthenticationError(errorCode, errString);
            }

            @Override
            public void onAuthenticationHelp(int helpCode,
                                             CharSequence helpString) {
                super.onAuthenticationHelp(helpCode, helpString);
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }

            @Override
            public void onAuthenticationSucceeded(
                    BiometricPrompt.AuthenticationResult result) {
                   startDrawerActivity();
                super.onAuthenticationSucceeded(result);
            }
        };
    }

    private CancellationSignal getCancellationSignal() {

        cancellationSignal = new CancellationSignal();
        cancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener() {
                                                           @Override
                                                           public void onCancel() {
                                                               notifyUser("Cancelled via signal");
                                                           }
                                                       });
        return cancellationSignal;
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    public void authenticateUser() {
        BiometricPrompt biometricPrompt = new BiometricPrompt.Builder(this)
                .setTitle("Verify your FingerPrint to Continue.")
                .setNegativeButton("Cancel", this.getMainExecutor(),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                              //  notifyUser("Authentication cancelled");
                            }
                        })
                .build();
        biometricPrompt.authenticate(getCancellationSignal(), getMainExecutor(), getAuthenticationCallback());
    }


    private void startDrawerActivity()
    {
        Intent intent=new Intent(this, Drawer_Activity.class);
        startActivity(intent);
        finish();
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.btnAuthenticate:
            {
                authenticateUser();
                break;
            }
            default:
            {
                break;
            }
        }

    }

    private void initSharedPref()
    {
        sharedPreferences = this.getSharedPreferences(Constants.ISFRINGERPRINTENABLED, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void statusBarColor(String colorCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(colorCode));
        }
    }
}