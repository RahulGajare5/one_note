package com.rg.one_note.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.rg.one_note.Constants.Constants;
import com.rg.one_note.DataBase.DBAdapter;
import com.rg.one_note.R;
import com.rg.one_note.Utility.Utilities;
import com.rg.one_note.fragments.NoteFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Models.Note;

import static android.content.Intent.createChooser;

public class Create_Edit_Note_Activity extends AppCompatActivity implements View.OnClickListener,OnInitListener {

    private EditText etTitle, etContent;
    private RelativeLayout rlRootLayout;
    private String title, content;
    private DBAdapter dbAdapter;
    private Note note;
    private ImageView ivGoback, ivTrash, ivOptions,ic_Calander , ic_Share,ic_Speak,ic_whatsApp;
    private DrawerLayout drawer;
    private Button btn_noteYellow, btn_noteBlue, btn_noteOrange, btn_noteRed, btn_noteGreen, btn_noteViolet, btn_noteCyan, btn_notePink,
            btn_noteMaroon, btn_noteNeonGreen, btn_noteNeonBlue, btn_noteWhite;
    private Button btn_txtBlack, btn_txtBlue, btn_txtOrange, btn_txtRed, btn_txtGreen, btn_txtViolet, btn_txtCyan, btn_txtPink,
            btn_txtMaroon, btn_txtNeonGreen, btn_txtNeonBlue, btn_txtWhite;
    private String noteColor = "#FFFFFF";
    private String textColor = "#000000";
    private CalendarView calender;
    private LinearLayout frame_calender,llLastEdited;
    private int MY_DATA_CHECK_CODE = 0;
    private TextToSpeech myTTS;
    private NavigationView navView;
    private Switch themeSwitch;
    private long lastClickTime = 0;
    private TextView tvLastEdited;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.colors_nav_drawer);
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, MY_DATA_CHECK_CODE);

        // TODO: 30-05-2020 Create singleton for DBadapter instance.
        dbAdapter = new DBAdapter(this);
        initViews();
        setClickListenrs();

        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        Intent intent = getIntent();
        note = (Note) intent.getSerializableExtra("NoteObject");
        if (note != null) {
            Log.d("Note", "onCreate: " + note.toString());
            initValuesForViews();
            statusBarColor(note.getNoteColor());
        }
        else
        {
                llLastEdited.setVisibility(View.GONE);
        }


    }

    private void initViews() {
        etTitle = findViewById(R.id.etTitle);
        etContent = findViewById(R.id.etContent);
        ivGoback = findViewById(R.id.ic_goback);
        ivTrash = findViewById(R.id.ic_trash);
        ivOptions = findViewById(R.id.ic_options);
        drawer = findViewById(R.id.drawer_layout);
        rlRootLayout =findViewById(R.id.rlRootLayout);
        btn_noteYellow = findViewById(R.id.btn_noteYellow);
        btn_noteBlue = findViewById(R.id.btn_noteBlue);
        btn_noteOrange = findViewById(R.id.btn_noteOrange);
        btn_noteRed = findViewById(R.id.btn_noteRed);
        btn_noteGreen = findViewById(R.id.btn_noteGreen);
        btn_noteViolet = findViewById(R.id.btn_noteViolet);
        btn_noteCyan = findViewById(R.id.btn_noteCyan);
        btn_notePink = findViewById(R.id.btn_notePink);
        btn_noteNeonBlue = findViewById(R.id.btn_noteNeonblue);
        btn_noteNeonGreen = findViewById(R.id.btn_noteNeongreen);
        btn_noteMaroon = findViewById(R.id.btn_noteMaroon);
        btn_noteWhite = findViewById(R.id.btn_noteWhite);

        btn_txtBlack =findViewById(R.id.btn_txtblack);
        btn_txtBlue =findViewById(R.id.btn_txtblue);
        btn_txtOrange =findViewById(R.id.btn_txtorange);
        btn_txtRed =findViewById(R.id.btn_txtred);
        btn_txtGreen =findViewById(R.id.btn_txtgreen);
        btn_txtViolet =findViewById(R.id.btn_txtviolet);
        btn_txtCyan =findViewById(R.id.btn_txtcyan);
        btn_txtPink =findViewById(R.id.btn_txtpink);
        btn_txtMaroon =findViewById(R.id.btn_txtmaroon);
        btn_txtNeonBlue =findViewById(R.id.btn_txtneonblue);
        btn_txtNeonGreen =findViewById(R.id.btn_txtneongreen);
        btn_txtWhite =findViewById(R.id.btn_txtwhite);
        ic_Share = findViewById(R.id.ic_Share);
        ic_Speak = findViewById(R.id.ic_Speak);
        themeSwitch = findViewById(R.id.switchTheme);
        navView =findViewById(R.id.nav_view);
        ic_whatsApp = findViewById(R.id.ic_Whatsapp);

        llLastEdited = findViewById(R.id.llLastEdited);
        tvLastEdited = findViewById(R.id.tvLastEdited);
    }

    private void setClickListenrs() {
        ivGoback.setOnClickListener(this);
        ivTrash.setOnClickListener(this);
        ivOptions.setOnClickListener(this);
        btn_noteYellow.setOnClickListener(this);
        btn_noteBlue.setOnClickListener(this);
        btn_noteOrange.setOnClickListener(this);
        btn_noteRed.setOnClickListener(this);
        btn_noteGreen.setOnClickListener(this);
        btn_noteViolet.setOnClickListener(this);
        btn_noteCyan.setOnClickListener(this);
        btn_notePink.setOnClickListener(this);
        btn_noteMaroon.setOnClickListener(this);
        btn_noteNeonGreen.setOnClickListener(this);
        btn_noteNeonBlue.setOnClickListener(this);
        btn_noteWhite.setOnClickListener(this);

        btn_txtBlack.setOnClickListener(this);
        btn_txtBlue.setOnClickListener(this);
        btn_txtOrange.setOnClickListener(this);
        btn_txtRed.setOnClickListener(this);
        btn_txtGreen.setOnClickListener(this);
        btn_txtViolet.setOnClickListener(this);
        btn_txtCyan.setOnClickListener(this);
        btn_txtPink.setOnClickListener(this);
        btn_txtMaroon.setOnClickListener(this);
        btn_txtNeonBlue.setOnClickListener(this);
        btn_txtNeonGreen.setOnClickListener(this);
        btn_txtWhite.setOnClickListener(this);

        ic_Share.setOnClickListener(this);
        ic_Speak.setOnClickListener(this);
        themeSwitch.setOnClickListener(this);
        ic_whatsApp.setOnClickListener(this);


    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    private void initValuesForViews() {
        etTitle.setText(note.getTitle());
        etContent.setText(note.getContent());
        rlRootLayout.setBackgroundColor(Color.parseColor(note.getNoteColor()));
        etContent.setTextColor(Color.parseColor(note.getTextColor()));
        noteColor = note.getNoteColor();
        textColor = note.getTextColor();
        tvLastEdited.setText(note.getDateStamp().toString());


    }


    private void initValuesForNote() {
        title = etTitle.getText().toString().trim();
        content = etContent.getText().toString().trim();

    }

    @Override
    public void onBackPressed() {

        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();
        initValuesForNote();

        if (note != null) {
            editNote(false);

        } else {
            createNewNote(false);
        }

        super.onBackPressed();

    }

    private String dateFormatter()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return formatter.format(date);
    }


    private void createNewNote(boolean isInBIn) {
        if (validate(title, content)) {
            Note note = new Note();
            note.setTitle(title);
            note.setContent(content);
            note.setNoteColor(this.noteColor);
            note.setTextColor(this.textColor);
            note.setDateStamp(dateFormatter());
            note.setPinned(false);

            if(isInBIn)
            {
                long row = dbAdapter.insertNoteInBin(note);
//                dbAdapter.deleteNote(note);
//                int position = getindexByproperty(note.getNote_id());
//                NoteFragment.noteList.remove(position);
            }
            else
            {
                long row = dbAdapter.insertNote(note);
                note.setNote_id(row);

                NoteFragment.noteList.add(0,note);
                if (row > -1) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constants.ACTION_NOTE, Constants.IS_NEW_NOTE);
                    setResult(this.RESULT_OK, returnIntent);
                } else {
                    Utilities.showToast(this, "Something wrong in your device.Note cant be saved!");
                }
            }



//        Snackbar.make(rlRootLayout, "Your note is saved.", Snackbar.LENGTH_LONG).show();
        }
    }

    private void editNote(boolean isInBIn) {
        note.setTitle(title);
        note.setContent(content);
        note.setNoteColor(this.noteColor);
        note.setTextColor(this.textColor);
        note.setDateStamp(dateFormatter());
        note.setPinned(false);
        if(isInBIn)
        {
            long  row =   dbAdapter.deleteNote(note);
            int position = getindexByproperty(note.getNote_id());
            NoteFragment.noteList.remove(position);
            note.setNote_id(null);
            dbAdapter.insertNoteInBin(note);
            Log.d("list", "editNote: "+ NoteFragment.noteList);
        }
        else {
            dbAdapter.updateNote(note);
            int position = getindexByproperty(note.getNote_id());
            NoteFragment.noteList.set(position,note);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.ACTION_NOTE, Constants.IS_EDITED_NOTE);
            setResult(this.RESULT_OK, returnIntent);
        }

    }


    private void addNoteToBin() {
        initValuesForNote();

        if (note != null) {
            editNote(true);

        } else {
            if (validate(title, content)) {
                createNewNote(true);
            }
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.ACTION_NOTE, Constants.IS_DELETED_NOTE);
        setResult(this.RESULT_OK, returnIntent);
        finish();
    }

    public void statusBarColor(String colorCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(colorCode));
        }
    }


    public boolean validate(String title, String content) {
        if (title.trim().equals("") && content.trim().equals("")) {
            return false;
        }
        return true;
    }


    private int getindexByproperty(long noteID) {
        for (int i = 0; i < NoteFragment.noteList.size(); i++) {
            if (NoteFragment.noteList.get(i).getNote_id() == noteID) {
                return i;
            }
        }

        return -1;
    }

    private void share()
    {
        /*Create an ACTION_SEND Intent*/
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        /*This will be the actual content you wish you share.*/
        String share_subject = etTitle.getText().toString() ;
        String shareBody = etTitle.getText().toString() +System.lineSeparator() + etContent.getText().toString();
        /*The type of the content is text, obviously.*/
        intent.setType("text/plain");
        /*Applying information Subject and Body.*/
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, share_subject);
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        /*Fire!*/
        startActivity(createChooser(intent,"Share using"));
    }

    private void shareViaWhatsApp()
    {
        Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
        String share_subject = etTitle.getText().toString() ;
        String shareBody = etTitle.getText().toString() +System.lineSeparator() + etContent.getText().toString();
        whatsappIntent.setType("text/plain");
        whatsappIntent.setPackage("com.whatsapp");
        whatsappIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, share_subject);
        whatsappIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
        try {
            startActivity(whatsappIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Utilities.showToast(this,"Whatsapp have not been installed.");
        }
    }

    private void shareviaGmail()
    {

    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.ic_trash: {
                addNoteToBin();
                break;
            }
            case R.id.ic_options: {
                drawer.openDrawer(Gravity.RIGHT);

                break;
            }

            case R.id.ic_goback:
            {
                onBackPressed();
                break;
            }

            case R.id.btn_noteYellow: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#F7DC6F"));
                noteColor = "#F7DC6F";
                statusBarColor("#F7DC6F");
                break;
            }

            case R.id.btn_noteBlue: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#5DADE2"));
                noteColor = "#5DADE2";
                statusBarColor("#5DADE2");
                break;
            }

            case R.id.btn_noteOrange: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#EB984E"));
                noteColor = "#EB984E";
                statusBarColor("#EB984E");
                break;
            }
            case R.id.btn_noteRed: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#EC7063"));
                noteColor = "#EC7063";
                statusBarColor("#EC7063");
                break;
            }
            case R.id.btn_noteGreen: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#2ECC71"));
                noteColor = "#2ECC71";
                statusBarColor("#2ECC71");
                break;
            }
            case R.id.btn_noteViolet: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#9B59B6"));
                noteColor = "#9B59B6";
                statusBarColor("#9B59B6");
                break;
            }
            case R.id.btn_noteCyan: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#10F2EB"));
                noteColor = "#10F2EB";
                statusBarColor("#10F2EB");
                break;
            }
            case R.id.btn_notePink: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#ff77ff"));
                noteColor = "#ff77ff";
                statusBarColor("#ff77ff");
                break;
            }
            case R.id.btn_noteMaroon: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#bf4080"));
                noteColor = "#bf4080";
                statusBarColor("#bf4080");
                break;
            }
            case R.id.btn_noteNeonblue: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#0F52BA"));
                noteColor = "#0F52BA";
                statusBarColor("#0F52BA");
                break;
            }
            case R.id.btn_noteNeongreen: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#00FF41"));
                noteColor = "#00FF41";
                statusBarColor("#00FF41");
                break;
            }
            case R.id.btn_noteWhite: {
                rlRootLayout.setBackgroundColor(Color.parseColor("#ffffff"));
                noteColor = "#ffffff";
                statusBarColor("#ffffff");
                break;
            }

            case R.id.btn_txtblack: {
                etContent.setTextColor(getResources().getColor(R.color.color_Black));
                textColor = getString(R.string.color_Black);
                break;
            }
            case R.id.btn_txtblue: {
                etContent.setTextColor(getResources().getColor(R.color.color_Blue));
                textColor = getString(R.string.color_Blue);
                break;
            }
            case R.id.btn_txtorange: {
                etContent.setTextColor(getResources().getColor(R.color.color_Orange));
                textColor = getString(R.string.color_Orange);
                break;
            }
            case R.id.btn_txtred: {
                etContent.setTextColor(getResources().getColor(R.color.color_Red));
                textColor = getString(R.string.color_Red);
                break;
            }
            case R.id.btn_txtgreen: {
                etContent.setTextColor(getResources().getColor(R.color.color_Green));
                textColor = getString(R.string.color_Green);
                break;
            }
            case R.id.btn_txtviolet: {
                etContent.setTextColor(getResources().getColor(R.color.color_Violet));
                textColor = getString(R.string.color_Violet);
                break;
            }
            case R.id.btn_txtcyan: {
                etContent.setTextColor(getResources().getColor(R.color.color_Cyan));
                textColor = getString(R.string.color_Cyan);
                break;
            }
            case R.id.btn_txtpink: {
                etContent.setTextColor(getResources().getColor(R.color.color_Pink));
                textColor = getString(R.string.color_Pink);
                break;
            }
            case R.id.btn_txtmaroon: {
                etContent.setTextColor(getResources().getColor(R.color.color_Maroon));
                textColor = getString(R.string.color_Maroon);
                break;
            }
            case R.id.btn_txtneonblue: {
                etContent.setTextColor(getResources().getColor(R.color.color_NeonBlue));
                textColor = getString(R.string.color_NeonBlue);
                break;
            }
            case R.id.btn_txtneongreen: {
                etContent.setTextColor(getResources().getColor(R.color.color_NeonGreen));
                textColor = getString(R.string.color_NeonGreen);
                break;
            }
            case R.id.btn_txtwhite: {
                etContent.setTextColor(getResources().getColor(R.color.color_White));
                textColor = getString(R.string.color_White);
                break;
            }

            case R.id.ic_Share: {
                share();
                break;
            }
            case R.id.ic_Whatsapp: {
                shareViaWhatsApp();
                break;
            }

            case R.id.ic_Speak: {

                String strRead = etContent.getText().toString();
                if(strRead.trim().equals(""))
                {
                    Utilities.showToast(this,"No text to read.");
                }
                else {
                    speakWords(strRead);
                }
                break;
            }

            case R.id.switchTheme:
            {
                if(themeSwitch.isChecked())
                {
                    navView.setBackgroundColor(Color.parseColor("#404040"));
                }
                else
                {
                    navView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                }
                break;
            }

            default:
            {
                break;
            }

        }

    }

    private void speakWords(String speech) {
        myTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                myTTS = new TextToSpeech(this, this);
            } else {
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onInit(int initStatus) {
        if (initStatus == TextToSpeech.SUCCESS) {
            myTTS.setLanguage(Locale.US);
        }
        else if (initStatus == TextToSpeech.ERROR) {
            Toast.makeText(this, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
        }
    }
}
