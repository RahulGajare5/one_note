package com.rg.one_note.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.rg.one_note.Constants.Constants;
import com.rg.one_note.DataBase.DBAdapter;
import com.rg.one_note.Helper.NotifyWorker;
import com.rg.one_note.Interfaces.IReminderPopupListener;
import com.rg.one_note.R;
import com.rg.one_note.Utility.Utilities;
import com.rg.one_note.Views.PopUp_Reminder;
import com.rg.one_note.fragments.NoteFragment;
import com.rg.one_note.fragments.TaskFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.xml.transform.Result;

import Models.Note;
import Models.Task;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public class TaskActivity extends AppCompatActivity  implements View.OnClickListener, IReminderPopupListener {

    private EditText etTask,etDetails;
    private Task task;
    private String strTask,details;
    private DBAdapter dbAdapter;
    private ImageView ivGoBack,ic_tick,ic_Delete,ic_reminder;
    private long lastClickTime = 0;
    private final static String task_notification_channel = "Task Event Reminder";
    public static final String workTag = "notificationWork";
    private long duration;
    private RelativeLayout rlRootLayout;
    private boolean isFromNotificationClick;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        dbAdapter = new DBAdapter(this);
        initViews();
        setClickListenrs();

        Intent intent = getIntent();
        task = (Task) intent.getSerializableExtra("TaskObject");
        if(task == null)
        {
            task = (Task) intent.getSerializableExtra("FromNotification");
            if(task!= null)
            {
                isFromNotificationClick = true;
            }
        }

        if (task != null) {
            initValuesForViews();
            createNotificationChannel();

        }
        else {
            ic_tick.setVisibility(View.GONE);
            ic_reminder.setVisibility(View.GONE);
        }

    }

    private void initValuesForViews()
    {
        etTask.setText(task.getContent());
        etDetails.setText(task.getDetails());
        if(isFromNotificationClick)
        {
            diableEditing();
            ic_tick.setVisibility(View.GONE);
            ic_reminder.setVisibility(View.GONE);
            ic_Delete.setVisibility(View.GONE);
            ivGoBack.setVisibility(View.GONE);
        }
        if(Utilities.parseIntToBoolean(task.getIsCompleted()))
        {
            ic_tick.setVisibility(View.GONE);
            ic_reminder.setVisibility(View.GONE);
            etTask.setPaintFlags(etTask.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            etDetails.setPaintFlags(etDetails.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            diableEditing();

        }
    }
    private void diableEditing()
    {
        etTask.setKeyListener(null);
        etDetails.setKeyListener(null);
    }

    private void initViews()
    {
        etTask = findViewById(R.id.etTask);
        etDetails = findViewById(R.id.etDetails);
        ivGoBack = findViewById(R.id.ic_goback);
        ic_tick = findViewById(R.id.ic_tick);
        ic_Delete =findViewById(R.id.ic_delete);
        ic_reminder = findViewById(R.id.ic_reminder);
        rlRootLayout = findViewById(R.id.rlRootLayout);
    }

    private void setClickListenrs()
    {
        ivGoBack.setOnClickListener(this);
        ic_tick.setOnClickListener(this);
        ic_Delete.setOnClickListener(this);
        ic_reminder.setOnClickListener(this);
    }
    @Override
    public void onBackPressed() {

            initValuesForTask();
        if (SystemClock.elapsedRealtime() - lastClickTime < 1000){
            return;
        }
        lastClickTime = SystemClock.elapsedRealtime();

            if (task != null) {

                if(!Utilities.parseIntToBoolean(task.getIsCompleted())) {
                    editTask(false);
                }

            } else {
                createNewTask(false);
            }



        super.onBackPressed();

    }

    private void initValuesForTask()
    {
        strTask = etTask.getText().toString().trim();
        details = etDetails.getText().toString().trim();
    }

    private void createNewTask(boolean isComleted)
    {
        try {
            if (validate(strTask,details)) {
                Task task = new Task();
                task.setCreatedDate(dateFormatter());
                task.setIsCompleted(0);
                task.setContent(strTask);
                task.setDetails(details);
                task.setReminderDate_Time("");

                if(isComleted)
                {
                    task.setIsCompleted(1);
                    long row = dbAdapter.insertTaskInCompleted(task);
                    task.setIsCompleted(1);
                    task.setTask_id(row);
                    TaskFragment.taskListCompleted.add(0,task);
                }
                else
                {
                    long row = dbAdapter.insertTask(task);
                    task.setTask_id(row);

                    TaskFragment.taskList.add(0,task);
                    if (row > -1) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(Constants.ACTION_TASK, Constants.IS_NEW_TASK);
                        setResult(this.RESULT_OK, returnIntent);
                    } else {
                        Utilities.showToast(this, "Something wrong in your device.Note cant be saved!");
                    }
                }

            }
        }
        catch(Exception ex)
        {
            ex.getMessage();
        }

    }

    private void editTask(boolean isCompleted) {
        task.setContent(strTask);
        task.setDetails(details);
        task.setCreatedDate(dateFormatter());
        task.setReminderDate_Time("");
        if(isCompleted)
        {
            dbAdapter.deleteTask(task);
            int position = getindexByproperty(task.getTask_id(), TaskFragment.taskList);
            TaskFragment.taskList.remove(position);
            task.setTask_id(null);
            long row =dbAdapter.insertTaskInCompleted(task);
            task.setIsCompleted(1);
            task.setTask_id(row);
            TaskFragment.taskListCompleted.add(0,task);
        }
        else {
            dbAdapter.updateTask(task);
            int position = getindexByproperty(task.getTask_id(), TaskFragment.taskList);
            TaskFragment.taskList.set(position,task);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.ACTION_TASK, Constants.IS_EDITED_TASK);
            setResult(this.RESULT_OK, returnIntent);
        }
    }

    private void addTaskToCompleted()
    {
        initValuesForTask();

        if (task != null) {
            editTask(true);

        } else {
            if (validate(strTask,details)) {
                createNewTask(true);
            }
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.ACTION_TASK, Constants.IS_COMPLETED_TASK);
        setResult(this.RESULT_OK, returnIntent);
        finish();
    }

    private void deleteTask() {

        if (task != null) {
            if(Utilities.parseIntToBoolean(task.getIsCompleted()))
            {
                dbAdapter.deleteTaskFromCompleted(task);
            }
            else {
                dbAdapter.deleteTask(task);
            }
        }


        Intent returnIntent = new Intent();
        returnIntent.putExtra(Constants.ACTION_NOTE, Constants.IS_DELETED_NOTE);
        setResult(this.RESULT_OK, returnIntent);
        finish();
    }


    private String dateFormatter()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return formatter.format(date).toString();
    }

    public boolean validate(String content, String details ) {
        if (content.trim().equals("") && details.trim().equals("")) {
            return false;
        }
        return true;
    }

    private int getindexByproperty(long taskID, List<Task> taskList) {
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getTask_id() == taskID) {
                return i;
            }
        }

        return -1;
    }

    public void showDialog()
    {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this,R.style.MyDialogTheme);
        builder1.setMessage("Do you want to remove this Task?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        deletePermanently();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();


    }

    private void deletePermanently()
    {
        if(task == null)
        {
            finish();
        }else {
            if (Utilities.parseIntToBoolean(task.getIsCompleted())) {
                dbAdapter.deleteTaskFromCompleted(task);
                int position = getindexByproperty(task.getTask_id(), TaskFragment.taskListCompleted);
                TaskFragment.taskListCompleted.remove(position);
            } else {
                dbAdapter.deleteTask(task);
                int position = getindexByproperty(task.getTask_id(), TaskFragment.taskList);
                TaskFragment.taskList.remove(position);
            }

            Intent returnIntent = new Intent();
            returnIntent.putExtra(Constants.ACTION_TASK, Constants.IS_PERMANENT_DELETED_TASK);
            setResult(this.RESULT_OK, returnIntent);
            finish();
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.ic_goback:
            {
                onBackPressed();
                break;
            }
            case R.id.ic_delete:
            {
                showDialog();
                break;
            }

            case R.id.ic_tick:
            {
                addTaskToCompleted();
                break;
            }

            case R.id.ic_reminder:
            {
              popReminderView(view);
                break;
            }

            default:
            {
                break;
            }
        }
    }

    public void createNotificationChannel()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //define the importance level of the notification
            int importance = NotificationManager.IMPORTANCE_HIGH;

            //build the actual notification channel, giving it a unique ID and name
            NotificationChannel channel =
                    new NotificationChannel(task_notification_channel, task_notification_channel, importance);

            String description = "A channel which shows notifications about Tasks ToDO";
            channel.setDescription(description);
            channel.setLightColor(Color.MAGENTA);

            NotificationManager notificationManager = (NotificationManager) getApplicationContext().
                    getSystemService(this.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

    }

    @Override
    public void DateAndTimeFromPopUp(long duration) {
        this.duration = duration;
       // this.selectedMinuteInMillis = selectedMinuteInMillis;
        createWorkRequest(task.getTask_id());


    }

    @Override
    public void OnSaveClicked() {

    }



    public  void createWorkRequest(long task_id)
    {
        String taskNotificationTag = "taskTag";
        Data inputData = new Data.Builder().putLong(taskNotificationTag, task.getTask_id()).build();

        OneTimeWorkRequest notificationWork = new OneTimeWorkRequest.Builder(NotifyWorker.class)
                .setInitialDelay(calculateDelay(), TimeUnit.MILLISECONDS)
                .setInputData(inputData)
                .addTag(workTag)
                .build();

        WorkManager.getInstance(this).enqueue(notificationWork);
        Snackbar.make(rlRootLayout, "Reminder Saved.", Snackbar.LENGTH_SHORT).show();
    }


    private long calculateDelay()
    {
        //return this.selectedDateInMillis + this.selectedMinuteInMillis ;
        return duration - 60000;
    }
    private void popReminderView(View v)
    {
        PopUp_Reminder popUpClass = new PopUp_Reminder();
        popUpClass.showPopupWindow(v,this,this);

    }

}