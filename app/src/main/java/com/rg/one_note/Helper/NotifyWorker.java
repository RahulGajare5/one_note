package com.rg.one_note.Helper;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.rg.one_note.DataBase.DBAdapter;
import com.rg.one_note.R;
import com.rg.one_note.activity.TaskActivity;

import Models.Task;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

public class NotifyWorker extends Worker {
    private DBAdapter dbAdapter;
    private final static String task_notification_channel = "Task Event Reminder";

        public NotifyWorker(@NonNull Context context, @NonNull WorkerParameters params) {
            super(context, params);
            dbAdapter = new DBAdapter(context);
        }

    @NonNull
        @Override
        public Result doWork() {
           Data data = getInputData();
          Long task_id= data.getLong("taskTag",-1);
            Task task = dbAdapter.getTaskById(task_id);
            triggerNotification(task);
            return Result.success();
        }

    private void  triggerNotification(Task task)
    {
        Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("FromNotification",task);

        PendingIntent pendingIntent =
                PendingIntent.getActivity(getApplicationContext(), 1, intent, FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(getApplicationContext(), task_notification_channel)
                        .setSmallIcon(R.mipmap.app_icon)
                        .setContentTitle(task.getContent())
                        .setContentText(task.getDetails())
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_HIGH);

        //trigger the notification
        NotificationManagerCompat notificationManager =
                NotificationManagerCompat.from(getApplicationContext());

        //we give each notification the ID of the event it's describing,
        //to ensure they all show up and there are no duplicates
        notificationManager.notify((int)(long)1, notificationBuilder.build());
    }

}
