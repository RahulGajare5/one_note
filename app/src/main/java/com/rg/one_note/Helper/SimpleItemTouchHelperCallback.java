package com.rg.one_note.Helper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.rg.one_note.Interfaces.ItemTouchHelperAdapter;
import com.rg.one_note.fragments.NoteFragment;

import Models.Note;

import static androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_DRAG;

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {
    private final ItemTouchHelperAdapter mAdapter;
    public static boolean draggable = true;
    private boolean isforTask;

    public SimpleItemTouchHelperCallback(ItemTouchHelperAdapter mAdapter, boolean isforTask) {
        this.mAdapter = mAdapter;
        this.isforTask = isforTask;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        if(isforTask)
        {
            return false;
        }
        else
        {
            return draggable;
        }

    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return false;
    }

    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        int dragFlags;
        if(isforTask)
        {
             dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        }
        else {
             dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN | ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT;
        }
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
        super.onSelectedChanged(viewHolder, actionState);
        if (actionState == ACTION_STATE_DRAG)
        {
            viewHolder.itemView.setAlpha(0.5f);
        }
    }


    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        viewHolder.itemView.setAlpha(1.0f);


    }

    public void getNotePosition()
    {
        for(int i = 0 ;  i < NoteFragment.noteList.size() ; i++)
        {

        }
    }
}
