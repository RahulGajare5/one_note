package com.rg.one_note.Constants;

 public  class Constants {
    public static  final String DATA_BASE_NAME = "ONE_NOTE";
     public static  final int DATA_BASE_VERSION = 1;



    //Notes Table Column names Constant
    public static  final String NAME_NOTES_TABLE = "NOTES";
  public static  final String NAME_BIN_NOTES_TABLE = "Bin_notes";
     public static  final String NOTE_ID = "_id";
     public static  final String NOTE_TITLE = "title";
     public static  final String NOTE_CONTENT = "content";
     public static  final String NOTE_COLOR = "noteColor";
     public static  final String TEXT_COLOR = "textColor";
     public static  final String NOTE_IS_PINNED = "isPinned";
     public static final String NOTE_DATE_STAMP = "dateStamp";
     public static final String NOTE_META_DATA = "metaData";
    public static final String NOTE_DRAGGED_POSITION = "DraggedPosition";

     //Task Table Column names Constant
     public static  final String NAME_TASK_TABLE = "task";
     public static  final String NAME_TASK_COMPLETED_TABLE = "completed_task";
     public static  final String TASK_ID = "_id";
     public static  final String TASK_COLOR = "taskColor";
     public static final String TASK_DATE_STAMP = "dateStamp";
     public static final String TASK_REMINDER_DATE_TIME = "reminder_date_time";
     public static final String TASK_CONTENT = "task_content";
     public static final String TASK_DETAILS = "task_details";
     public static final String TASK_ISCOMPLETED = "is_completed";
     public static final String TASK_DRAGGED_POSITION = "DraggedPosition";



     //this strings to determine how (what action )the user is coming back from Create_Edit_Note_Activity
     //cases :-
     // 1)By creating a new note and pressing back button.
     //2)By editing a already exist note and pressing back button.
     //3) By clicking trash/delete.

     public static final String ACTION_NOTE = "action_note";
     public static final String IS_NEW_NOTE = "is_new_note";
     public static final String IS_EDITED_NOTE = "is_edited_note";
     public static final String IS_DELETED_NOTE = "is_deleted_note";
     public static final String IS_PERMANENT_DELETED_NOTE = "is_permanentdeleted_note";
     public static final String IS_RESTORED_NOTE = "is_restored_note";

     public static final String THEME_PREFERENCE = "theme_preference";
     public static final String FINGERPRINT_PREFERENCE = "fingerprint_preference";
     public static final String ISFRINGERPRINTENABLED = "isfingerprintenabled";
     public static final String IS_DARK_THEME = "is_dark_theme";
     public static final String IS_LIGHT_THEME = "is_light_theme";

     public static final String ACTION_TASK = "action_task";
     public static final String IS_NEW_TASK = "is_new_task";
     public static final String IS_EDITED_TASK = "is_edited_task";
     public static final String IS_COMPLETED_TASK = "is_deleted_task";
     public static final String IS_PERMANENT_DELETED_TASK = "is_permanentdeleted_task";
     public static final String IS_RESTORED_TASK = "is_restored_task";


 }
