package com.rg.one_note.Interfaces;

import androidx.drawerlayout.widget.DrawerLayout;

public interface ActivityCommunication {

    public DrawerLayout getDrawer();
}
