package com.rg.one_note.Interfaces;

public interface IReminderPopupListener {

    public void  DateAndTimeFromPopUp(long duration);
    public void  OnSaveClicked();
}
