package com.rg.one_note.Interfaces;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public interface RecyclerViewClickListener {

    public void rowItemClicked(View v, int position);
    public void requestDrag(RecyclerView.ViewHolder viewHolder);
    public void checkBoxClicked(View v ,int position);
}
