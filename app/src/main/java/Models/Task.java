package Models;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Task implements Serializable {
    private Long task_id;
    private int isCompleted;
    private String content;
    private String createdDate;
    private String details;
    private long draggedPosition;

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(int isCompleted) {
        this.isCompleted = isCompleted;
    }

    public long getDraggedPosition() {
        return draggedPosition;
    }

    public void setDraggedPosition(long draggedPosition) {
        this.draggedPosition = draggedPosition;
    }

    public Long getTask_id() {
        return task_id;
    }

    public void setTask_id(Long task_id) {
        this.task_id = task_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getReminderDate_Time() {
        return reminderDate_Time;
    }

    public void setReminderDate_Time(String reminderDate_Time) {
        this.reminderDate_Time = reminderDate_Time;
    }

    private String reminderDate_Time;
}
