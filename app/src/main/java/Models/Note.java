package Models;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Note implements Serializable {
    private Long note_id;
    private String title;
    private String content;
    private String noteColor;
    private String dateStamp;
    private boolean isPinned;
    private String metadata = "";
    private String textColor;
    private long draggedPosition;


    public boolean isPinned() {
        return isPinned;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public Note()
    {

    }

    public Long getNote_id() {
        return note_id;
    }

    public void setNote_id(Long note_id) {
        this.note_id = note_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public String getNoteColor() {
        return noteColor;
    }

    public void setNoteColor(String noteColor) {
        this.noteColor = noteColor;
    }

    public String getDateStamp() {
        return dateStamp;
    }

    public void setDateStamp(String dateStamp) {
        this.dateStamp = dateStamp;
    }

    public boolean getIsPinned() {
        return isPinned;
    }

    public void setPinned(boolean pinned) {
        isPinned = pinned;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public long getDraggedPosition() {
        return draggedPosition;
    }

    public void setDraggedPosition(long draggedPosition) {
        this.draggedPosition = draggedPosition;
    }

    @Override
    public String toString() {
        return "Note{" +
                "note_id=" + note_id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", noteColor='" + noteColor + '\'' +
                ", dateStamp='" + dateStamp + '\'' +
                ", isPinned=" + isPinned +
                ", metadata='" + metadata + '\'' +
                ", textColor='" + textColor + '\'' +
                ", draggedPosition=" + draggedPosition +
                '}';
    }
}
